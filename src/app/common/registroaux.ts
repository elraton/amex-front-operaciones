export interface RegistroAux {
  id: number;
  categoria: string;
  debe: string;//
  descripcion: string;
  porpagar: boolean;
  tasacambio: string;
  tipo: string;  
  haber: string;//
  conforme: boolean;
  enviado: boolean;
  viaje: string;
  operacion: string;
  fechaenviado: string;
  proveedor: string;
  fecha: string;
  documentos: string;
  orden: string;
  adelanto: boolean;
  porrendir: boolean;
  aux: string;
}
