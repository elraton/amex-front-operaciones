import { Document } from './document';
export interface Resumen {
    budget_s: number;
    budget_d: number;
    category: string;
    diference_s: string;
    diference_d: string;
    documents: Document[];
    percentage_s: number;
    percentage_d: number;
    real_s: string;
    real_d: string;
}
