export interface CashNotClosed {
	amount: string;
	type: string;
	year: string;
}