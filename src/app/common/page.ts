export interface Page {
	page: number;
	state: string;
}