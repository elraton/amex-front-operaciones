export interface Provider {
    id: number;
    name: string;
    socialreason: string;
    address: string;
    ruc: string;
    category: number;
    telephone: string;
    email: string;
    rate: string;
    logo: string;
    type: string;
    active: string;
}
