export interface Budget {
    id: number;
    trip: string;
    category: string;
    amount: string;
    currency: string;
}
