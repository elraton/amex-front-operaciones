export interface ViajesAux {
  planilla: string;
  resumen: string;
  codigo: string;
  fecha: string;
  fechafin: string;
  operaciones: boolean;
  gerencia: boolean;
  tipo: string;
  calificacion: string;
  aux: string;
}
