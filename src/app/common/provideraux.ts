export interface ProviderAux {
    nombre: string;
    socialreason: string;
    direccion: string;
    ruc: string;
    telefono: string;
    contacto: string;
    category: number;
    calificacion: string;
    logo: string;
    aux: string;
}
