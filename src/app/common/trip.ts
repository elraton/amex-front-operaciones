export interface Trip {
	id: number;
	code: string;
	description: string;
	date: string;
	type: string;
	date_end: string;
	rate: string;
	operations: boolean;
	management: boolean;
	agency: string;
	active: string;
}
