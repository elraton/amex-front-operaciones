export interface ReportProvider {
	id: number;
	agree: boolean;
	amount: string;
	currency: string;
	category: string;
	date: string;
	description: string;
	document_number: string;
	document_photo: string;
	document_type: string;
	flag: string;
	provider: string;
	provider_origin: string;
	send: boolean;
	send_date: string;
	trip: string;
	cheque_number: string;
	exchange_rate: string;
	operation_type: string;
	type: string;
}