export interface Register {
	id: number;
	active: string;
	order_number: string;
    trip: string;
    date: string;
    operation_type: string;
    category: string;
    provider: string;
    description: string;
    cheque_number: string;
    exchange_rate: string;
    type: string;
    amount: string;
    residue: string;
    debt: string;
    send: boolean;
    send_date: string;
    agree: boolean;
    to_pay: boolean;
    advance: boolean;
    to_adjust: boolean;
    documents: string;
}
