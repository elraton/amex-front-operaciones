import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UtilidadesComponent } from './utilidades.component';
import { UtilidadesRoutes } from './utilidades.routing';
import {SharedModule} from '../shared/shared.module';

import { UtilidadesBComponent } from './utilidadesb/utilidadesb.component';
import { AddUtilidadesComponent } from './addutilidades/addutilidades.component';
import { EditUtilidadesComponent } from './editutilidades/editutilidades.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UtilidadesRoutes),
      SharedModule
  ],
  declarations: [
  	UtilidadesComponent,
  	UtilidadesBComponent,
  	AddUtilidadesComponent,
  	EditUtilidadesComponent
  ]
})

export class UtilidadesModule {}
