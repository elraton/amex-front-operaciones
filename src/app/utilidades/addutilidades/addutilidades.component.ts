import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-utilidades',
  templateUrl: './addutilidades.component.html',
  styleUrls: ['./addutilidades.component.css']
})

export class AddUtilidadesComponent implements OnInit {

	name: string;
	type: string;
	util_name: string;

	constructor(
		private router: Router,
    	private location: Location,
    	private route: ActivatedRoute,
			private http: HttpClient,
			private toastyService: ToastyService,
		private toastyConfig: ToastyConfig
		) {
			this.toastyConfig.theme = 'bootstrap';
		 }

	ngOnInit() {
		this.name = '';
		this.route.params.subscribe(params => {
			this.type = params['t'];
	    });

	    if(this.type == '1') {
	    	this.util_name = 'Tipo de viaje';
	    }
	    if(this.type == '2') {
	    	this.util_name = 'Categoria';
	    }
	    if(this.type == '3') {
	    	this.util_name = 'Tipo de operación';
	    }
	    if(this.type == '4') {
	    	this.util_name = 'Tipo de documento';
	    }
	}

	save() {
		if(this.name != '') {
			let data = {
				'type': this.type,
				'name': this.name
			};

			this.http.post(localStorage.getItem('baseurl') + 'add_utilities/', data)
		      .subscribe(
		        res => {
		          console.log(res);
		          this.location.back();
		        },
		        error => {
		          console.log(error);

		          if(
		          	error.error.text === 'New trip type created' ||
		          	error.error.text === 'New category created' ||
		          	error.error.text === 'New operation type created' ||
		          	error.error.text === 'New document type created'
		          	) {
									this.toastyService.success('Registro Creado');
									setTimeout(() => {
										this.router.navigate(['/utilidades']);
									}, 1000);
		          }
		        }
		      );
		} else {
			this.toastyService.warning('Debe ingresar un nombre');
		}
	}
	cancelar() {
    this.location.back();
  }
}
