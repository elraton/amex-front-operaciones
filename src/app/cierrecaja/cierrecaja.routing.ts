import { Routes } from '@angular/router';

import { CierreCajaComponent } from './cierrecaja.component';

import { AuthGuard } from '../guard';

export const CierreCajaRoutes: Routes = [
	{
	  path: '',
	  canActivate: [AuthGuard],
	  component: CierreCajaComponent,
	  data: {
	    breadcrumb: 'Cierre Cajas',
	    icon: 'icofont-home bg-c-blue', 
	    status: false
	  }
	}
];

/*export const ReportesRoutes: Routes = [{
  path: '',
  component: ReportesComponent,
  data: {
    breadcrumb: 'Reportes',
    status: true
  },
  children: [
      {
        path: ':type/:provider',
          component: ReportesComponent,
          data: {
              breadcrumb: 'Reportes',
              status: false
          }
      }
    ]
}];*/