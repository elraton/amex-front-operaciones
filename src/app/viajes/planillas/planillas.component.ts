import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { RegistroAux } from '../../common/registroaux'

import { Page } from '../../common/page';
import { Register } from '../../common/register';
import {IMyDpOptions} from 'mydatepicker';
import {ViajesAux} from '../../common/viajeaux';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './planillas.component.html',
  styleUrls: ['./planillas.component.css']
})
export class PlanillasComponent implements OnInit {

  displayedColumns = [
    'select',
    'fecha',
    'viaje',
    'operacion',
    'proveedor',
    'tipdoc',
    'tipo',
    'debe',
    'haber',
    'categoria',
    'enviado',
    'porpagar',
    'adelanto',
    'porrendir',
    'conforme',
    'aux'
  ];

  baseimg2: string = localStorage.getItem('baseassets');

  dataSource;

  ELEMENT_DATA: RegistroAux[];

  @ViewChild(MatSort) sort: MatSort;

  selection;

  isLoadingResults: boolean;

  id: number;

  fecha: string;
  proveedores: string;
  categoria: string;

  Registros: Register[];
  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  filterSelect: number;

  dates1: string;
  datee1: string;

  proveedoresv: string;
  categoriav: string;

  proveedores_list: any[];
  categoria_list: any[];

  showNotFound: string;

  Trip: string;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: false,
    selectorWidth: '400px',
  };

  public date1: any = { date: { year: 0, month: 0, day: 0 } };
  public date2: any = { date: { year: 0, month: 0, day: 0 } };



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {

    this.isLoadingResults = true;

    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    let formData = {};

    formData = {
      'type': '4',
      'trip': this.id
    };

    this.http.post(localStorage.getItem('baseurl') + 'list_documents/filter/?page=1', formData)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.Registros = JSON.parse(resaux).objects;
          this.totalpages = JSON.parse(resaux).num_pages;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i + 1 === this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          if(this.Registros.length < 1) {
            this.showNotFound = 'show-loading';
          }
        },
        error => {
          console.log(error);
        });

    this.proveedores_list = JSON.parse(localStorage.getItem('suppliers'));
    this.categoria_list = JSON.parse(localStorage.getItem('categories'));

  	this.fecha = 'no-display';
	  this.proveedores = 'no-display';
	  this.categoria = 'no-display';

    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;

    this.proveedoresv = '0';
    this.categoriav = '0';

    this.Trip = '';

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          for(let xx of JSON.parse(resaux).trips) {
            if(this.id == xx.id){
              this.Trip = xx.code;
            }
          }
        },
        error => {
          console.log(error);
        });


    this.route.params.subscribe(params => {
      this.page = +params['p'];
    });

    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  showDocuments(docs) {
    console.log(docs);
    let content = '';
    for(let xx of JSON.parse(JSON.stringify(docs))){
      content = content + '<div class="documentPopup" [ngStyle]="{\'border\':\'2px dotted\'},{\'padding\':\'15px\'},{\'margin-bottom\':\'15px\'}">';
      content = content + '<p><strong>Fecha: </strong>'+xx.date.split('T')[0]+'</p>';
      content = content + '<p><strong>Descripción: </strong>'+xx.description+'</p>';
      content = content + '<p><strong>Monto: </strong>'+xx.amount+'</p>';
      content = content + '<p><strong>Tipo de Documento: </strong>'+xx.document_type+'</p>';
      content = content + '<p><strong>Número de Documento: </strong>'+xx.document_number+'</p>';
      content = content + '</div>';
    }
    this.modalService.openDialog(this.viewRef, {
      title: 'Documentos',
      childComponent: SimpleModalComponent,
      data: {
        text: content
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Cerrar',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        }
      ]
    });
  }

  createTableRegisters() {
    this.ELEMENT_DATA = [];

    for ( let xx of this.Registros) {
      let debe = 0;
      let haber = 0;
      if (Number(xx.amount) > 0){
        haber = Number(xx.amount);
      } else {
        if (Number(xx.amount) == 0){
          debe = Number(xx.amount);
        } else {
          debe = Number(xx.amount) * -1;
        }
        
      }

      console.log('debe ' + debe);
      console.log('haber ' + haber);
      this.ELEMENT_DATA.push(
        {
            id: xx.id,
            categoria: xx.category,
            debe: debe.toString(),//
            haber: haber.toString(),//
            descripcion: xx.description,
            porpagar: xx.to_pay,
            tasacambio: xx.exchange_rate,
            tipo: xx.type,
            conforme: xx.agree,
            enviado: xx.send,
            viaje: xx.trip,
            operacion: xx.operation_type,
            fechaenviado: xx.send_date,
            proveedor: xx.provider,
            fecha: xx.date,
            documentos: xx.documents,
            orden: xx.order_number,
            adelanto: xx.advance,
            porrendir: xx.to_adjust,
            aux: xx.id.toString(),
        }
      );
    }

    this.isLoadingResults = false;
    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource = new MatTableDataSource<RegistroAux>(this.ELEMENT_DATA);
    this.selection = new SelectionModel<RegistroAux>(true, []);
    this.dataSource.sort = this.sort;
  }

  changePage(p) {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/registro/"+this.page);

    let formData = {};

    formData = {
      'type': '4',
      'trip': this.id
    };

    this.http.post(localStorage.getItem('baseurl') + 'list_documents/filter/?page='+this.page, formData)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.Registros = JSON.parse(resaux).objects;
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';

          this.createTableRegisters();

          if (this.Registros.length < 1) {
            this.showNotFound = 'show-loading';
          }
        },
        error => {
          console.log(error);
        });
  }

  editTrip(v) {
    let vv;
    for (let xx of this.Registros) {
      if (xx.id.toString() === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/registro/edit/'+v]);
        return;
      }
    }
  }
  viewTrip(v) {
    let vv;
    for (let xx of this.Registros) {
      if (xx.id.toString() === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/registro/view/'+v]);
        return;
      }
    }
  }

  deleteTrip(v) {
    let vv;
    for (let xx of this.Registros) {
      if (xx.id.toString() === v) {
        vv = xx;
        break;
      }
    }
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Registro?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const varData = new FormData();
              varData.append('id', v);
              this.http.post(localStorage.getItem('baseurl') + 'list_documents/delete/', varData)
                .subscribe(
                  res => {
                    console.log(res);
                    //this.location.back();
                  },
                  error => {
                    console.log(error.error.text);
                    if(error.error.text === 'Register deleted.') {
                      resolve();
                      this.Registros = this.Registros.filter(obj => obj !== vv);
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }
  activeTrip(v) {
    for(let x of this.Registros) {
      x.active = '';
    }
    v.active = 'active';
  }

  filtrar() {
    if (this.date1.formatted !== undefined) {
      this.dates1 = this.date1.formatted;
    }
    if (this.date2.formatted !== undefined) {
      this.datee1 = this.date2.formatted;
    }
    switch (+this.filterSelect) {
      case 1:{
        if(this.dates1 != undefined && this.datee1 != undefined) {
          this.router.navigate(['/registro/filter/1/' + this.dates1 + '/' + this.datee1 + '/1']);
        }
        break;
      }
      case 2:{
        this.router.navigate(['/registro/filter/2/'+this.proveedoresv+'/1']);
        break;
      }
      case 3:{
        this.router.navigate(['/registro/filter/3/'+this.categoriav+'/1']);
        break;
      }
      default:
        // code...
        break;
    }
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

  filtrarForm(e, val) {
    switch (val) {
      case "0":{
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        break;
      }
      case "1":{
        this.fecha = '';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        break;
      }
      case "2":{
        this.fecha = 'no-display';
        this.proveedores = '';
        this.categoria = 'no-display';
        break;
      }
      case "3":{
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = '';
        break;
      }
      default:
        // code...
        break;
    }
  }

}
