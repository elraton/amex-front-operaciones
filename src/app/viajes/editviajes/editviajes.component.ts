import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { Trip } from '../../common/trip';
import { Budget } from '../../common/budget';
import {IMyDpOptions} from 'mydatepicker';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './editviajes.component.html',
  styleUrls: ['./editviajes.component.css']
})
export class EditViajesComponent implements OnInit {

  Viajes: any;

  Budgets: Budget[];

  myControl2: FormControl = new FormControl();
  options2 = [];

  filteredOptions2: Observable<string[]>;

  list_categories: any[];
  list_categories_copy: any[];


  baseimg2: string = localStorage.getItem('baseassets');

  id: number;

  list_types: any[];
  trip: Trip;
  trip_aux: Trip;

  rol = JSON.parse(localStorage.getItem('rol'));
  showbudget = this.rol.trip_budget;

  list_providers: any[];
  code: string;
  description: string;
  date: string;
  type: string;
  date_end: string;
  rate: number;
  operations: boolean;
  management: boolean;

  showLoading: string;

  trip_status: number;
  trip_operations: number;
  trip_management: number;

  showSpin: string;
  isSaved: boolean;
  isloading: boolean;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px',
  };

  public date1: any;
  public date2: any;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {

    this.isloading = true;

    this.Budgets = [];
    this.showSpin = 'hideLoading';
    this.isSaved = false;

    this.showLoading = 'showLoading';

    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    this.list_types = JSON.parse(localStorage.getItem('trip_types'));

    this.trip = JSON.parse(localStorage.getItem("edittrip"));
    this.trip_aux = this.trip;

    this.list_categories = JSON.parse(localStorage.getItem('categories'));
    this.list_categories_copy = [];

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.list_providers = JSON.parse(aux).providers;

          for(let xx of this.list_providers) {
            this.options2.push(xx.name);
          }

        this.filteredOptions2 = this.myControl2.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

    this.code = this.trip.code;
    this.description = this.trip.description;
    this.date = this.trip.date;

    this.type = '0';
    for (const t of this.list_types) {
      if (t.name === this.trip.type) {
        this.type = t.id;
      }
    }

    for (const xx of this.list_categories) {
      this.list_categories_copy.push(
        {
          name: xx.name,
          id: xx.id,
          disable: false
        }
      );
    }

    let aux;
    if(this.date != null){
      aux = this.date.split('-');
      this.date1 = { date: { year: +aux[0], month: +aux[1], day: +aux[2] } };
    }

    if(this.date_end != null){
      aux = this.date_end.split('-');
      this.date2 = { date: { year: +aux[0], month: +aux[1], day: +aux[2] } };
    }

    this.rate = +this.trip.rate;
    this.operations = this.trip.operations;
    this.management = this.trip.management;

    if(this.operations) {
      this.trip_operations = 2;
    } else {
      this.trip_operations = 1;
    }

    if(this.management) {
      this.trip_management = 2;
    } else {
      this.trip_management = 1;
    }

    if(this.rate == null) {
      this.rate = 0;
    }

    const data = new FormData();
    data.append('trip', this.id.toString());
    this.http.post(localStorage.getItem('baseurl') + 'list_budgets/', data)
      .subscribe(
        res => {
          this.Budgets = JSON.parse(JSON.stringify(res)).objs;
          if (this.Budgets.length > 0) {
            for (let xx of this.Budgets) {
              xx.category = this.parseCategory(xx.category);
              this.filterCategories(xx.category);
            }
          }
        },
        error => {
          console.log(error.error.text);
        }
      );

    this.showLoading = 'hideLoading';

  }

  parseCategory(c) {
    for (let xx of this.list_categories) {
      if (c === xx.name) {
        return xx.id;
      }
    }
  }

  starsClick(x) {
    this.rate = x;
  }

  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  setRate(x: number) {
    this.rate = x;
  }

  canDeactivate() {
    if (this.isSaved === false) {
      this.isSaved = true;
      if ( this.trip.code !== this.code ) {
        this.isSaved = false;
      }
      if ( this.trip.description !== this.description ) {
        this.isSaved = false;
      }
      let type;
      for (const t of this.list_types) {
        if (t.name === this.trip.type) {
          type = t.id;
        }
      }
      if ( type !== this.type ) {
        this.isSaved = false;
      }
      if (this.isSaved === false) {
        return window.confirm('¿Realmente Desea Salir?');
      }
    }
    return true;
  }

  addBudget() {
    this.Budgets.push(
      {
        id: 0,
        trip: this.id.toString(),
        category: '0',
        amount: '',
        currency: 'D'
      }
    );
  }

  deleteBudget(b) {
    let popup = false;
    if (Number(b.amount) === 0) {
      if (b.id === 0) {
        popup = false;
        this.Budgets = this.Budgets.filter(obj => obj !== b);
      } else {
        popup = true;
      }
    } else {
      popup = true;
    }

    if (popup) {
      this.modalService.openDialog(this.viewRef, {
        title: '¿Seguro que quieres eliminar el Presupuesto?',
        childComponent: SimpleModalComponent,
        data: {
          text: ''
        },
        settings: {
          closeButtonClass: 'close theme-icon-close'
        },
        actionButtons: [
          {
            text: 'Si',
            buttonClass: 'btn btn-danger',
            onAction: () => new Promise((resolve: any, reject: any) => {
              setTimeout(() => {
                const data = new FormData();
                data.append('id', b.id);
                this.http.post(localStorage.getItem('baseurl') + 'list_budgets/delete/', data)
                  .subscribe(
                    res => {
                      console.log(res);
                      this.Budgets = this.Budgets.filter(obj => obj !== b);
                      resolve();
                    },
                    error => {
                      console.log(error.error.text);
                    }
                  );
              }, 20);
            })
          },
          {
            text: 'No',
            buttonClass: 'btn btn-success',
            onAction: () => new Promise((resolve: any) => {
              setTimeout(() => {
                resolve();
              }, 20);
            })
          },
        ]
      });
    }
  }

  toggleCurrency(b) {
    if (b.currency === 'S') {
      b.currency = 'D';
    } else {
      b.currency = 'S';
    }
  }

  filterCategories(b) {
    for (let xx of this.list_categories_copy) {
      if (+b === xx.id) {
        xx.disable = true;
      }
    }
  }

  text2upper(event: any) {
    const inputChar = String.fromCharCode(event.charCode);
    this.code = this.code.toUpperCase();
  }

  save() {
    // this.showSpin = '';

    this.trip.id = this.id;

    this.trip.code = this.code;
    this.trip.description = this.description;

    this.trip.type = this.type;

    if(this.date1 !== undefined && this.date1 !== null){
      if (this.date1.formatted !== undefined) {
        this.trip.date = this.date1.formatted.split('/')[2] + '-' + this.date1.formatted.split('/')[1] + '-' + this.date1.formatted.split('/')[0];
      } else {
        this.trip.date = this.date;
      }
    } else {
      this.trip.date = this.date;
    }

    if(this.date2 !== undefined && this.date2 !== null){
      if (this.date2.formatted !== undefined) {
        this.trip.date_end = this.date2.formatted;
      } else {
        this.trip.date_end = '';
      }
    } else {
      this.trip.date_end = '';
    }

    this.trip.rate = this.rate.toString();

    if(this.trip_operations === 1) {
      this.trip.operations = false;
    } else {
      this.trip.operations = true;
    }

    if(this.trip_management === 1) {
      this.trip.management = false;
    } else {
      this.trip.management = true;
    }

    this.trip.agency = '';

    let error: boolean = false;

    if (this.trip.code.length < 3) {
      this.toastyService.warning('Ingrese un código');
      error = true;
    }

    if (this.trip.date === '') {
      this.toastyService.warning('Ingrese una Fecha de Inicio de Viaje');
      error = true;
    }

    if (this.trip.type === '0') {
      this.toastyService.warning('Seleccione un Tipo de Viaje');
      error = true;
    }

    if(error === false) {
      const formData = new FormData();
      formData.append('id', this.trip.id.toString());
      formData.append('code', this.trip.code);
      formData.append('description', this.trip.description);
      formData.append('date', this.trip.date);
      formData.append('type', this.trip.type);
      formData.append('date_end', this.trip.date_end);
      formData.append('rate', this.trip.rate);
      formData.append('operations', this.trip.operations ? '1' : '0');
      formData.append('management', this.trip.management ? '1' : '0');
      formData.append('agency', this.trip.agency);

      this.http.post(localStorage.getItem('baseurl') + 'list_trips/edit/', formData)
      .subscribe(
        res => {
          console.log(res);
          //this.location.back();
          this.toastyService.success('Viaje editado');
          this.Budgets = this.Budgets.filter(obj => Number(obj.amount) !== 0);
          if (this.Budgets.length > 0) {
            for (let xx of this.Budgets) {
              xx.trip = this.id.toString();
            }

            const newBudgets = this.Budgets.filter(bb => bb.id === 0);
            const editBudget = this.Budgets.filter(bb => bb.id !== 0);

            if (newBudgets.length > 0) {
              const newBud = new FormData();
              newBud.append('budget', JSON.stringify(newBudgets));
              this.http.post(localStorage.getItem('baseurl') + 'list_budgets/add/', newBud)
                .subscribe(
                  res2 => {
                    this.toastyService.success('Presupuestos Nuevos Guardados');
                    if (editBudget.length > 0) {
                      const editBud = new FormData();
                      editBud.append('budget', JSON.stringify(editBudget));
                      this.http.post(localStorage.getItem('baseurl') + 'list_budgets/edit/', editBud)
                        .subscribe(
                          res3 => {
                            this.toastyService.success('Presupuestos Editados');
                            this.isSaved = true;
                            setTimeout(() => {
                              this.location.back();
                            }, 1000);
                          },
                          error1 => {
                            console.log(error1.error.text);
                            this.toastyService.warning('Ocurrio un error al editar los Presupuestos');
                          }
                        );
                    } else {
                      this.isSaved = true;
                      setTimeout(() => {
                        this.location.back();
                      }, 1000);
                    }
                  },
                  error1 => {
                    console.log(error1.error.text);
                    this.toastyService.warning('Ocurrio un error al guardar los Presupuestos');
                  }
                );
            } else {
              if (editBudget.length > 0) {
                const editBud = new FormData();
                editBud.append('budget', JSON.stringify(editBudget));
                this.http.post(localStorage.getItem('baseurl') + 'list_budgets/edit/', editBud)
                  .subscribe(
                    res3 => {
                      this.toastyService.success('Presupuestos Editados');
                      this.isSaved = true;
                      setTimeout(() => {
                        this.location.back();
                      }, 1000);
                    },
                    error1 => {
                      console.log(error1.error.text);
                      this.toastyService.warning('Ocurrio un error al editar los Presupuestos');
                    }
                  );
              } else {
                this.isSaved = true;
                setTimeout(() => {
                  this.location.back();
                }, 1000);
              }
            }
          } else {
            this.isSaved = true;
            setTimeout(() => {
              this.location.back();
            }, 1000);
          }
        },
        error3 => {
          console.log(error3.error.text);
        }
      );
    }


  }

  goback() {
    this.location.back();
  }

  cancelar() {
    this.location.back();
  }

}
