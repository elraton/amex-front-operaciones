import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

import { Page } from '../../common/page';
import { Register } from '../../common/register';
import {IMyDpOptions} from 'mydatepicker';

import { Resumen } from '../../common/resumen';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.css']
})
export class ResumenComponent implements OnInit {

  id: number;
  Trip: string;
  showLoading: string;
  showTableData: string;

  resumen: Resumen[];

  baseimg2: string = localStorage.getItem('baseassets');

  disableOperation;
  disableManage;
  closeOperations: boolean;
  closeManagement: boolean;

  closing = false;

  SumBudget_s = 0;
  SumReal_s = 0;
  SumDiff_s = 0;

  SumBudget_d = 0;
  SumReal_d = 0;
  SumDiff_d = 0;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    ) { }

  ngOnInit() {

    this.closeOperations = localStorage.getItem('operations') === '1' ? true : false;
    this.closeManagement = localStorage.getItem('management') === '1' ? true : false;


    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    const formData = new FormData();

    formData.append('trip', this.id.toString());

    this.http.post(localStorage.getItem('baseurl') + 'abstract_trip/', formData)
      .subscribe(
        response => {
          this.resumen = JSON.parse(JSON.stringify(response)).objs;
          console.log(this.resumen);
          this.resumen = this.sortData( this.resumen );
          for ( const xx of this.resumen) {
            this.SumBudget_s = this.SumBudget_s + Number(xx.budget_s);
            this.SumReal_s = this.SumReal_s + Number(xx.real_s);
            this.SumDiff_s = this.SumDiff_s + Number(xx.diference_s);

            this.SumBudget_d = this.SumBudget_d + Number(xx.budget_d);
            this.SumReal_d = this.SumReal_d + Number(xx.real_d);
            this.SumDiff_d = this.SumDiff_d + Number(xx.diference_d);
          }
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
        },
        error => {
          console.log(error);
        });

    this.Trip = '';

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          for(let xx of JSON.parse(resaux).trips) {
            if(this.id == xx.id){
              this.Trip = xx.code;
            }
          }
        },
        error => {
          console.log(error);
        });
  }

  formatNumber(ss) {
    return Number(ss).toFixed(2);
  }

  sortData( arr) {
    const data = this.resumen.slice();

    return arr = data.sort((a, b) => {
      const isAsc = true;
      return compare(a.category, b.category, isAsc);
    });
  }

  Closetrip() {
    this.disableOperation = true;
    this.closing = true;

    const form = new FormData();
    form.append('trip', this.id.toString());
    form.append('operations', this.closeOperations ? '1' : '0');
    form.append('management', this.closeManagement ? '1' : '0');

    this.http.post(localStorage.getItem('baseurl') + 'close_trip/', form)
    .subscribe(
      res => {
        this.toastyService.success('Viaje guardado');
        this.disableOperation = false;
        this.closing = false;
      },
      error => {
        console.log(error.error.text);
        this.toastyService.error('No se pudo guardar');
      }
    );
  }

  filterCurrency(cc) {
    if (cc === 'S') {
      return 'S/.';
    } else {
      return '$';
    }
  }

  goback() {
    this.location.back();
  }

  cutText(ss: string, size: number) {
    if (ss.length > size) {
      return ss.slice(0, 20) + '...';
    } else {
      return ss;
    }
  }

  openDocuments(res: Resumen) {
    let total_s = 0;
    let total_d = 0;
    let content = '';
    content += '<table class="mat-table mat-elevation-z8 document_popup_table">';
    content += '<thead>';
    content += '<tr class="mat-header-row">';
    content += '<th class="mat-header-cell"> Fecha </th>';
    content += '<th class="mat-header-cell"> Proveedor </th>';
    content += '<th class="mat-header-cell"> Tipo Doc </th>';
    content += '<th class="mat-header-cell"> Num Doc </th>';
    content += '<th class="mat-header-cell"> Soles </th>';
    content += '<th class="mat-header-cell"> Dolares </th>';
    content += '</tr>';
    content += '</thead>';
    content += '<tbody>';
    for (const xx of res.documents) {
      if (xx.currency === 'S') {
        total_s = total_s + Number(xx.amount);
      } else {
        total_d = total_d + Number(xx.amount);
      }
      content += '<tr class="mat-row">';
      content += '<td class="mat-cell">' + xx.date + '</td>';
      content += '<td class="mat-cell">' + this.cutText(xx.provider_origin, 10) + '</td>';
      content += '<td class="mat-cell">' + this.cutText(xx.document_type, 20) + '</td>';
      content += '<td class="mat-cell">' + xx.document_number + '</td>';
      content += '<td class="mat-cell">' + (xx.currency === 'S' ? xx.amount : '') + '</td>';
      content += '<td class="mat-cell">' + (xx.currency === 'D' ? xx.amount : '') + '</td>';
      content += '</tr>';
    }
    content += '<tr class="mat-row">';
    content += '<td colspan="3" class="mat-cell"></td>';
    content += '<td class="mat-cell">Total: </td>';
    content += '<td class="mat-cell">' + total_s.toFixed(2) + '</td>';
    content += '<td class="mat-cell">' + total_d.toFixed(2) + '</td>';
    content += '</tr>';
    content += '</tbody></table>';

    if (res.documents.length <= 0) {
      content = 'No contiene documentos';
    }
    this.modalService.openDialog(this.viewRef, {
      title: 'Documentos',
      childComponent: SimpleModalComponent,
      data: {
        text: content
      },
      settings: {
        closeButtonClass: 'close theme-icon-close',
        contentClass: 'popup_table_cont'
      },
    });
  }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}