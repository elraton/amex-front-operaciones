import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ViajesComponent } from './viajes.component';
import { ViajesRoutes } from './viajes.routing';
import { SharedModule } from '../shared/shared.module';

import { PlanillasComponent } from './planillas/planillas.component';
import { ResumenComponent } from './resumen/resumen.component';
import { ResumenDetalleComponent } from './resumendetalle/resumendetalle.component';
import { ViajesBComponent } from './viajes/viajesb.component';
import { EditViajesComponent } from './editviajes/editviajes.component';
import { AddViajesComponent } from './addviajes/addviajes.component';
import { ViewViajesComponent } from './viewviajes/viewviajes.component';
import { FilterViajesComponent } from './filter/filterviajes.component';

import { MyDatePickerModule } from 'mydatepicker';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CanDeactivateGuard } from '../preventexit/prevent';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ViajesRoutes),
      SharedModule,
      MyDatePickerModule,
      MatTableModule,
      MatSortModule,
      MatAutocompleteModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MatInputModule,
      MatSlideToggleModule
  ],
  declarations: [
    ViajesComponent,
    PlanillasComponent,
    ViajesBComponent,
    EditViajesComponent,
    AddViajesComponent,
    ViewViajesComponent,
    FilterViajesComponent,
    ResumenComponent,
    ResumenDetalleComponent
  ],
  providers: [
      CanDeactivateGuard
  ]
})

export class ViajesModule {}
