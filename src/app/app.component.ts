import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class AppComponent {
  constructor(
    public router: Router,
  ) {
    const token = localStorage.getItem('token');
    if (token === '') {
      // no hay token redirige a login
      console.log('el modulo');
      this.router.navigate(['/authentication/login']);
    }
  }
}
