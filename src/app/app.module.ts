import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { ModalDialogModule } from 'ngx-modal-dialog';
import { MyDatePickerModule } from 'mydatepicker';
import { MatTableModule } from '@angular/material/table';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import {ScrollModule} from './scroll/scroll.module';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';

import { AuthGuard } from './guard';

// import { NguiAutoCompleteModule } from '@ngui/auto-complete';

localStorage.setItem('baseurladm', 'http://amex.infoloop.net/web_adm/');
localStorage.setItem('baseurlope', 'http://amex.infoloop.net/web/');


// descomentar para produccion amex
localStorage.setItem('baseurl', '/web/');

// descomentar para desarrollo local
// localStorage.setItem('baseurl', 'http://amex.infoloop.net/web/');

localStorage.setItem('baseimg', 'http://amex.infoloop.net');

// descomentar para produccion amex
localStorage.setItem('baseassets', '/static/frontend/');

// descomentar para desarrollo local
// localStorage.setItem('baseassets', '');

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    BreadcrumbsComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes, {useHash: true}),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    ScrollModule,
    ModalDialogModule.forRoot(),
    MyDatePickerModule,
    MatTableModule
  ],
  exports: [
    ScrollModule,
    MatTableModule
  ],
  providers: [
      { provide: LocationStrategy, useClass: PathLocationStrategy },
      AuthGuard
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule {}
