import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Layout',
    main: [
      {
        state: 'registro',
        name: 'Registro de Documentos',
        type: 'link',
        icon: 'ti-home'
      },
      {
        state: 'dinero',
        name: 'Registro de movimientos',
        type: 'link',
        icon: 'ti-money'
      },
      {
        state: 'viajes',
        name: 'Viajes',
        type: 'link',
        icon: 'ti-bag'
      },
      {
        state: 'proveedores',
        name: 'Registro de proveedores',
        type: 'link',
        icon: 'ti-package'
      },
      {
        state: 'reportes',
        name: 'Reportes',
        type: 'link',
        icon: 'ti-bar-chart'
      },
      {
        state: 'cierrecaja',
        name: 'Cierre de Cajas',
        type: 'link',
        icon: 'ti-bar-chart'
      },
      {
        state: 'utilidades',
        name: 'Utilidades',
        type: 'link',
        icon: 'ti-settings'
      }
    ]
  }
];

const Menu_pers = MENUITEMS[0].main;



@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  reindex() {
    const formData = new FormData();
    formData.append('username', localStorage.getItem('username'));

    console.log('menu log');

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          const rol = JSON.parse(JSON.parse(JSON.stringify(res)).role)[0].fields;
          let menuaux = Menu_pers;
          if (!rol.doc_view) {
            menuaux = menuaux.filter( (ob) => {
              if (ob.state.indexOf('registro') === -1) {
                return ob;
              }
            });
          }

          if (!rol.mov_view) {
            menuaux = menuaux.filter( (ob) => {
              if (ob.state.indexOf('dinero') === -1) {
                return ob;
              }
            });
          }

          if (!rol.trip_view) {
            menuaux = menuaux.filter( (ob) => {
              if (ob.state.indexOf('viajes') === -1) {
                return ob;
              }
            });
          }

          if (!rol.prov_view) {
            menuaux = menuaux.filter( (ob) => {
              if (ob.state.indexOf('proveedores') === -1) {
                return ob;
              }
            });
          }

          if (!rol.report_mov &&
            !rol.report_prov_balance &&
            !rol.report_prov_adv &&
            !rol.report_to_back &&
            !rol.report_to_pay) {
            menuaux = menuaux.filter( (ob) => {
              if (ob.state.indexOf('reportes') === -1) {
                return ob;
              }
            });
          }

          MENUITEMS[0].main = menuaux;


        },
        error => {
          console.log(error);
        }
      );
  }

  constructor(
    private http: HttpClient
  ) {
    this.reindex();
  }
}
