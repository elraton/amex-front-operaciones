import { Routes } from '@angular/router';

import { ProveedoresComponent } from './proveedores.component';
import { ProveedoresBComponent } from './proveedoresb/proveedoresb.component';
import { EditProveedoresComponent } from './editproveedores/editproveedores.component';
import { AddProveedoresComponent } from './addproveedores/addproveedores.component';
import { ViewProveedoresComponent } from './viewproveedores/viewproveedores.component';
import { CanDeactivateGuard } from '../preventexit/prevent';

export const ProveedoresRoutes: Routes = [{
  path: '',
  component: ProveedoresComponent,
  data: {
    breadcrumb: 'Registro de proveedores',
    status: true
  },
  children: [
      {
        path: ':p',
          component: ProveedoresBComponent,
          data: {
              breadcrumb: 'Registro de proveedores',
              status: false
          }
      },
      {
        path: 'edit/:id',
          component: EditProveedoresComponent,
          data: {
              breadcrumb: 'Editar Proveedor',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/new',
          component: AddProveedoresComponent,
          data: {
              breadcrumb: 'Añadir Proveedor',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'view/:id',
          component: ViewProveedoresComponent,
          data: {
              breadcrumb: 'Ver Proveedor',
              status: false
          }
      }

    ]
}];
