import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Provider } from '../../common/provider';
import {IMyDpOptions} from 'mydatepicker';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  selector: 'app-add-viajes',
  templateUrl: './addproveedores.component.html',
  styleUrls: ['./addproveedores.component.css']
})
export class AddProveedoresComponent implements OnInit {

  name: string;
  socialreason: string;
  address: string;
  RUC: string;
  phone: string;
  contact: string;
  rate: number;
  logo_file: File;
  logo_url: string;
  logo: any;
  category = 0;

  isSaved = false;

  proveedor: Provider;

  type: string = '0';

  showImg: boolean;

  photo_base_64: string;

  list_categories: any[];

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) { 
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {
    this.name = '';
    this.address = '';
    this.socialreason = '';
    this.RUC = '';
    this.phone = '';
    this.contact = '';
    this.rate = 0;
    this.logo_url = '';
    this.showImg = false;
    this.photo_base_64 = '';

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
    .subscribe(
      res => {
        const aux = JSON.stringify(res);
        localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
        this.list_categories = JSON.parse(localStorage.getItem('categories'));
      },
      error => {
        console.log(error.error.text);
      }
    );
  }

  onChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      this.logo_file = event.target.files[0];
      reader.readAsDataURL(this.logo_file);
      reader.onload = () => {
        this.photo_base_64 = reader.result;
        this.showImg = true;
      };

    }
  }

  goback() {
    this.location.back();
  }

  canDeactivate() {
    console.log('i am navigating away');
    if(this.isSaved == false){
      return window.confirm('¿Realmente Desea Salir?');
    }
    return true;
  }

  starsClick(s) {
    this.rate = s;
  }

  cleanForm() {
    this.name = '';
    this.address = '';
    this.socialreason = '';
    this.RUC = '';
    this.phone = '';
    this.category = 0;
    this.contact = '';
    this.rate = 0;
    this.logo_url = '';
    this.showImg = false;
    this.photo_base_64 = '';
  }

  save(opt: Number) {

    let error = false;

    if(this.name === '') {
      error = true;
      this.toastyService.warning('Ingrese un nombre de proveedor');
    }

    if(this.type === '0') {
      error = true;
      this.toastyService.warning('Eliga un tipo de proveedor');
    }

    const formData = new FormData();

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    formData.append('name', this.name);
    formData.append('address', this.address);
    formData.append('socialreason', this.socialreason);
    formData.append('ruc', this.RUC);
    formData.append('telephone', this.phone);
    formData.append('category', this.category.toString());
    formData.append('email', this.contact);
    formData.append('rate', this.rate.toString());
    formData.append('logo', this.logo_file);
    formData.append('type', this.type);

    if (error === false) {
      this.http.post(localStorage.getItem('baseurl') + 'list_supplier/add/', formData, {headers: headers })
        .subscribe(
          res => {
            console.log(res);
            this.location.back();
          },
          error => {
            console.log(error.error.text);
            if(error.error.text === 'Provider created') {
              this.toastyService.success('Proveedor creado');
              this.isSaved = true;
              if ( opt === 1) {
                setTimeout(() => {
                  this.location.back();
                }, 1000);
              } else {
                this.cleanForm();
              }
            }

            if (error.error.text === 'Provider already exists') {
              this.toastyService.error('El Proveedor ya existe');
            }
          }
        );

    }
  }
  cancelar(){
    this.location.back();
  }

}
