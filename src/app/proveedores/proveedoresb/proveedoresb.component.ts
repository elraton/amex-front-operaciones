import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { Page } from '../../common/page';
import { Provider } from '../../common/provider';
import { ProviderAux } from '../../common/provideraux';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './proveedoresb.component.html',
  styleUrls: ['./proveedoresb.component.css']
})
export class ProveedoresBComponent implements OnInit {

  displayedColumns = [
    'select',
    'nombre',
    'socialreason',
    'direccion',
    'ruc',
    'telefono',
    'contacto',
    'calificacion',
    'logo',
    'aux'
  ];
  dataSource;

  ELEMENT_DATA: ProviderAux[];

  @ViewChild(MatSort) sort: MatSort;

  selection;

  isLoadingResults : boolean;

  Proveedores: Provider[];
  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  filterSelect: number;

  baseImg: string = localStorage.getItem('baseimg');
  baseimg2: string = localStorage.getItem('baseassets');

  code2search: string;


  filterdata = {
      'type': '1',
      'page': 1,
      'supplier_name': ''
    };
  isFiltering : boolean = false;
  isSearching: boolean = false;

  isLoading = true;
  showData = false;

  rol = JSON.parse(localStorage.getItem('rol'));
  showadd;
  showview;
  showedit;
  showdelete;
  showexport;

  items: Number;
  itemspertable = 20;

  list_categories: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {

    const formData = new FormData();
    formData.append('username', localStorage.getItem('username'));

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          this.rol = JSON.parse(JSON.parse(JSON.stringify(res)).role)[0].fields;
          localStorage.setItem('rol', JSON.stringify(this.rol));
          this.showadd = this.rol.prov_add;
          this.showview = this.rol.prov_view;
          this.showedit = this.rol.prov_edit;
          this.showdelete = this.rol.prov_delete;
          this.showexport = this.rol.prov_export;
        },
        error => {
          console.log(error);
        }
      );

    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.isLoadingResults = true;

    this.code2search = '';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
    .subscribe(
      res => {
        const aux = JSON.stringify(res);
        localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
        this.list_categories = JSON.parse(localStorage.getItem('categories'));
      },
      error => {
        console.log(error.error.text);
      }
    );


    this.route.params.subscribe(params => {
      this.page = +params['p'];
    });

    this.http.get(localStorage.getItem('baseurl') + 'list_suppliers/?page=' + this.page)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);

          this.totalpages = JSON.parse(resaux).num_pages;
          this.items = JSON.parse(resaux).items;
          this.Proveedores = JSON.parse(resaux).objects;
          console.log(this.Proveedores);
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }
          if(this.totalpages === 1) {
            this.disablePrev = 'disabled';
            this.disableNext = 'disabled';
          }

          if(this.page === this.totalpages) {
            this.disableNext = 'disabled';
          }
          if(this.page > 1) {
            this.disablePrev = '';
          }
          if(this.page < this.totalpages) {
            this.disableNext = '';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if ( this.Proveedores.length < 1) {
            this.isLoading = false;
            this.showData = false;
          }


        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });





  }

  setRows() {
    this.page = 1;
    this.location.replaceState('/proveedores/' + this.page);

    this.isLoading = true;
    this.showData = false;

    this.http.get(localStorage.getItem('baseurl') + 'list_suppliers/?page=' + this.page + '&items=' + this.itemspertable)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);

          this.totalpages = JSON.parse(resaux).num_pages;
          this.items = JSON.parse(resaux).items;
          this.Proveedores = JSON.parse(resaux).objects;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }
          if(this.totalpages === 1) {
            this.disablePrev = 'disabled';
            this.disableNext = 'disabled';
          }

          if(this.page === this.totalpages) {
            this.disableNext = 'disabled';
          }
          if(this.page > 1) {
            this.disablePrev = '';
          }
          if(this.page < this.totalpages) {
            this.disableNext = '';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if ( this.Proveedores.length < 1) {
            this.isLoading = false;
            this.showData = false;
          }


        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  ClearSearch(){
    this.isSearching = false;
    this.page = 1;
    this.isLoadingResults = true;
    this.isFiltering = false;
    this.code2search = '';
    this.isLoading = true;
    this.showData = false;
    this.http.get(localStorage.getItem('baseurl') + 'list_suppliers/?page='+this.page)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);

          this.totalpages = JSON.parse(resaux).num_pages;
          this.Proveedores = JSON.parse(resaux).objects;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }
          if(this.totalpages === 1) {
            this.disablePrev = 'disabled';
            this.disableNext = 'disabled';
          }

          if(this.page === this.totalpages) {
            this.disableNext = 'disabled';
          }
          if(this.page > 1) {
            this.disablePrev = '';
          }
          if(this.page < this.totalpages) {
            this.disableNext = '';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if (this.Proveedores.length < 1) {
            this.isLoading = false;
            this.showData = false;
          }


        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  searchChange(){
    if(this.code2search !== '') {
      if(this.code2search.length > 1){
        this.isSearching = true;
      }
    }
  }

  getCategory(id) {
    for (const xx of this.list_categories) {
      if (xx.id === id) {
        return xx.name;
      }
    }
  }

  searchCode(){
    this.isLoadingResults = true;
    this.isFiltering = true;
    this.filterdata.supplier_name = this.code2search;

    this.isLoading = true;
    this.showData = false;

    this.http.post(localStorage.getItem('baseurl') + 'list_suppliers/filter/', this.filterdata)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);

          this.totalpages = JSON.parse(resaux).num_pages;
          this.Proveedores = JSON.parse(resaux).objects;
          this.paginator = [];
          this.page = 1;
          
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          if(this.totalpages === 1) {
            this.disablePrev = 'disabled';
            this.disableNext = 'disabled';
          }

          if(this.page === this.totalpages) {
            this.disableNext = 'disabled';
          }
          if(this.page > 1) {
            this.disablePrev = '';
          }
          if(this.page < this.totalpages) {
            this.disableNext = '';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';

          this.isLoading = false;
          this.showData = true;

          if (this.Proveedores.length < 1) {
            this.isLoading = false;
            this.showData = false;
          }

        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  cutText(ss: string) {
    if (ss != null && ss.length > 0) {
      if (ss.length > 8) {
        return ss.slice(0, 10) + '...';
      } else {
        return ss;
      }
    } else {
      return '';
    }
  }

  createTableRegisters() {
    this.ELEMENT_DATA = [];

    for ( let xx of this.Proveedores) {
      this.ELEMENT_DATA.push(
        {
          nombre: xx.name,
          socialreason: xx.socialreason,
          direccion: xx.address,
          ruc: xx.ruc,
          telefono: xx.telephone,
          contacto: xx.email,
          category: xx.category,
          calificacion: xx.rate,
          logo: xx.logo,
          aux: xx.id.toString()
        }
      );
    }

    this.isLoadingResults = false;
    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource = new MatTableDataSource<ProviderAux>(this.ELEMENT_DATA);
    this.selection = new SelectionModel<ProviderAux>(true, []);
    this.dataSource.sort = this.sort;
  }

  selectChange(e: any, row: any) {
    e ? this.selection.toggle(row) : null;


    /*if(this.selection.hasValue()){
      this.activeExport = '';
    } else {
      this.activeExport = 'disabled';
    }*/
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

    /*if(this.selection.hasValue()){
      this.activeExport = '';
    } else {
      this.activeExport = 'disabled';
    }*/
  }

  changePage(p) {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.isLoading = true;
    this.showData = false;
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/proveedores/"+this.page);

    if(this.isFiltering){
      this.filterdata.page = this.page;
      this.http.post(localStorage.getItem('baseurl') + 'list_suppliers/filter/?page='+this.page, this.filterdata)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.Proveedores = JSON.parse(resaux).objects;

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if (this.Proveedores.length < 1) {
            this.isLoading = false;
            this.showData = false;
          }
        },
        error => {
          console.log(error);
          this.isLoading = false;
            this.showData = false;
        });
    } else {
      this.http.get(localStorage.getItem('baseurl') + 'list_suppliers/?page='+this.page)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            this.Proveedores = JSON.parse(resaux).objects;

            this.createTableRegisters();

            this.showLoading = 'hide-loading';
            this.showTableData = 'show-data';
            this.isLoading = false;
            this.showData = true;

            if (this.Proveedores.length < 1) {
              this.isLoading = false;
              this.showData = false;
            }
          },
          error => {
            console.log(error);
            this.isLoading = false;
            this.showData = false;
          });
    }

  }

  decode(s) {
    return s;
  }

  editTrip(v) {
    let prov: any;
    for (const xx of this.Proveedores) {
      if (xx.id === +v) {
        localStorage.setItem("edittrip", JSON.stringify(xx));
        this.router.navigate(['/proveedores/edit/' + v]);
        break;
      }
    }
  }
  viewTrip(v) {
    let prov: any;
    for (const xx of this.Proveedores) {
      if (xx.id === +v) {
        localStorage.setItem("edittrip", JSON.stringify(xx));
        this.router.navigate(['/proveedores/view/'+v]);
        break;
      }
    }
  }

  deleteTrip(v) {
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Proveedor?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const params = {
                'id': v.aux
              };
              this.http.post(localStorage.getItem('baseurl') + 'list_supplier/delete/', params)
                .subscribe(
                  res => {
                    console.log(res);
                    //this.location.back();
                  },
                  error => {
                    console.log(error.error.text);
                    if(error.error.text === 'Provider deleted.') {
                      resolve();
                      this.Proveedores = this.Proveedores.filter(obj => obj.id !== +v.aux);
                      this.createTableRegisters();
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }
  activeTrip(v) {
    for(let x of this.Proveedores) {
      x.active = '';
    }
    v.active = 'active';
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

}
