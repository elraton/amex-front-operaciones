import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ProveedoresComponent } from './proveedores.component';
import { ProveedoresRoutes } from './proveedores.routing';
import { SharedModule } from '../shared/shared.module';

import { ProveedoresBComponent } from './proveedoresb/proveedoresb.component';
import { EditProveedoresComponent } from './editproveedores/editproveedores.component';
import { AddProveedoresComponent } from './addproveedores/addproveedores.component';
import { ViewProveedoresComponent } from './viewproveedores/viewproveedores.component';

import { MyDatePickerModule } from 'mydatepicker';
import {NgxPaginationModule} from 'ngx-pagination';
import { CanDeactivateGuard } from '../preventexit/prevent';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProveedoresRoutes),
      SharedModule,
      MyDatePickerModule,
      NgxPaginationModule,
      MatTableModule,
      MatSortModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MatTooltipModule
  ],
  declarations: [
    ProveedoresComponent,
    ProveedoresBComponent,
    EditProveedoresComponent,
    AddProveedoresComponent,
    ViewProveedoresComponent
  ],
  providers: [
      CanDeactivateGuard
  ]
})

export class ProveedoresModule {}
