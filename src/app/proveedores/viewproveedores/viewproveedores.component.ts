import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { Provider } from '../../common/provider';
import {IMyDpOptions} from 'mydatepicker';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import {ModalDialogService, SimpleModalComponent} from 'ngx-modal-dialog';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './viewproveedores.component.html',
  styleUrls: ['./viewproveedores.component.css']
})
export class ViewProveedoresComponent implements OnInit {

  id: number;
  name: string;
  socialreason: string;
  address: string;
  RUC: string;
  phone: string;
  contact: string;
  rate: number;
  logo_file: File;
  logo_url: string;
  logo: any;

  logo_base64: string;
  showImg: boolean;
  showImgNot: boolean;

  proveedor: Provider;

  isSaved = false;

  baseimg2: string = localStorage.getItem('baseassets');

  baseImg: string = localStorage.getItem('baseimg');

  type: string = '0';

  category = 0;
  list_categories: any[];


  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private modalDialogService: ModalDialogService,
    private viewContainer: ViewContainerRef,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) {
    this.toastyConfig.theme = 'bootstrap';
     }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    this.proveedor = JSON.parse(localStorage.getItem('edittrip'));

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
    .subscribe(
      res => {
        const aux = JSON.stringify(res);
        localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
        this.list_categories = JSON.parse(localStorage.getItem('categories'));
      },
      error => {
        console.log(error.error.text);
      }
    );

    this.name = this.proveedor.name !== null ? this.proveedor.name : '';
    this.socialreason = this.proveedor.socialreason !== null ? this.proveedor.socialreason : '';
    this.address = this.proveedor.address !== null ? this.proveedor.address : '';
    this.RUC = this.proveedor.ruc.length > 0 ? this.proveedor.ruc : '';
    this.RUC = !isNaN(Number(this.proveedor.ruc)) ? this.proveedor.ruc : '';
    this.phone = this.proveedor.telephone !== null ? this.proveedor.telephone : '';
    this.contact = this.proveedor.email !== null ? this.proveedor.email : '';
    this.category = this.proveedor.category !== null ? this.proveedor.category : 0;
    this.rate = this.proveedor.rate !== null ? +this.proveedor.rate : 0;
    this.logo_url = this.proveedor.logo !== undefined ? this.proveedor.logo : '';
    this.showImg = false;
    this.showImgNot = false;
    this.type = this.proveedor.type !== null ? this.proveedor.type : 'A';

    if(this.logo_url === '') {
      this.showImgNot = true;
    } else {
      this.showImg = true;
    }
  }

  canDeactivate() {
    if (this.isSaved === false) {
      this.isSaved = true;
      if (this.name !== this.proveedor.name) {
        this.isSaved = false;
      }
      if (this.socialreason !== this.proveedor.socialreason) {
        this.isSaved = false;
      }
      if (this.address !== this.proveedor.address) {
        this.isSaved = false;
      }
      if (this.RUC !== this.proveedor.ruc) {
        this.isSaved = false;
      }
      if (this.phone !== this.proveedor.telephone) {
        this.isSaved = false;
      }
      if (this.contact !== this.proveedor.email) {
        this.isSaved = false;
      }
      if (this.rate !== Number(this.proveedor.rate) ) {
        this.isSaved = false;
      }
      if (this.type !== this.proveedor.type) {
        this.isSaved = false;
      }
      if (this.isSaved === false) {
        return window.confirm('¿Realmente Desea Salir?');
      }
    }
    return true;
  }

  goback() {
    this.location.back();
  }

  openImage(img: string) {
    this.modalDialogService.openDialog(this.viewContainer, {
      title: '',
      childComponent: SimpleModalComponent,
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      data: {
        text: '<img src="'+img+'">'
      }
    });
  }

  onChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      this.logo_file = event.target.files[0];
      reader.readAsDataURL(this.logo_file);
      reader.onload = () => {
        this.logo_base64 = reader.result;
        this.showImg = false;
        this.showImgNot = false;
      };

    }
  }

  starsClick(s) {
    this.rate = s;
  }

  save() {
    let error = false;

    if(this.name === '') {
      error = true;
      this.toastyService.warning('Ingrese un nombre de proveedor');
    }

    if(this.type === '0') {
      error = true;
      this.toastyService.warning('Eliga un tipo de proveedor');
    }

    const formData = new FormData();

    formData.append('id', this.id.toString());
    formData.append('name', this.name);
    formData.append('socialreason', this.socialreason);
    formData.append('address', this.address);
    formData.append('ruc', this.RUC);
    formData.append('telephone', this.phone);
    formData.append('category', this.category.toString());
    formData.append('email', this.contact);
    formData.append('rate', this.rate.toString());
    formData.append('logo', this.logo_file);
    formData.append('type', this.type);

    if(error === false) {
      this.http.post(localStorage.getItem('baseurl') + 'list_supplier/edit/', formData)
      .subscribe(
        res => {
          console.log(res);
          this.location.back();
        },
        error => {
          console.log(error.error.text);
          if(error.error.text === 'Provider updated') {
            this.isSaved = true;
            this.toastyService.success('Proveedor guardado');
            this.location.back();
          }
        }
      );
    }
  }
  cancelar(){
    this.location.back();
  }

}
