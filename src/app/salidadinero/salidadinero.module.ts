import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SalidaDineroComponent } from './salidadinero.component';
import { SalidaDineroRoutes } from './salidadinero.routing';
import { SharedModule } from '../shared/shared.module';

import { ShowSalidaDineroComponent } from './showsalidadinero/showsalidadinero.component';
import { AddSalidaDineroComponent } from './addsalidadinero/addsalidadinero.component';
import { EditSalidaDineroComponent } from './editsalidadinero/editsalidadinero.component';
import { ViewSalidaDineroComponent } from './viewsalidadinero/viewsalidadinero.component';
import { FilterSalidaDineroComponent } from './filtersalidadinero/filtersalidadinero.component';
import { AddIngresosComponent } from './addingresos/addingresos.component';

import { AddDevolucionesComponent } from './adddevoluciones/adddevoluciones.component';
import { AddTransferenciasComponent } from './addtransferencias/addtransferencias.component';

import { MyDatePickerModule } from 'mydatepicker';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CanDeactivateGuard } from '../preventexit/prevent';

import { PreviousRouteService } from './previousrouteservice';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(SalidaDineroRoutes),
      SharedModule,
      MyDatePickerModule,
      MatTableModule,
      MatSortModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MatAutocompleteModule,
      MatInputModule,
      MatTabsModule,
      MatTooltipModule
  ],
  declarations: [
    SalidaDineroComponent,
    ShowSalidaDineroComponent,
    AddSalidaDineroComponent,
    EditSalidaDineroComponent,
    ViewSalidaDineroComponent,
    AddDevolucionesComponent,
    AddTransferenciasComponent,
    FilterSalidaDineroComponent,
    AddIngresosComponent
  ],
  providers: [
      CanDeactivateGuard,
      PreviousRouteService
  ]
})

export class SalidaDineroModule {}
