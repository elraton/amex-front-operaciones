import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { Trip } from '../../common/trip';
import { Dinero } from '../../common/dinero';
import {IMyDpOptions} from 'mydatepicker';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  selector: 'app-view-viajes',
  templateUrl: './viewsalidadinero.component.html',
  styleUrls: ['./viewsalidadinero.component.css']
})


export class ViewSalidaDineroComponent implements OnInit {

  myControl: FormControl = new FormControl();
  myControl2: FormControl = new FormControl();

  options = [];
  options2 = [];

  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;

  list_trips: any[];
  list_providers: any[];
  list_tip_docs: any[];
  list_categories: any[];

  TripListStrings: string[];
  tripText: any;

  register: Dinero;
  registerDestino: Dinero;

  photo_base_64: string;
  photo_file: File;
  showImg: boolean;

  debehaber: number;

  isloading: boolean;
  isSaved: boolean;

  isDevolucion: boolean;
  isTransferencia: boolean;
  isNegative: boolean;

  baseimg2: string = localStorage.getItem('baseassets');
  
  isUploading: boolean = false;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: false,
    selectorWidth: '400px',
  };



  public date1: any;
  public date2: any;

  document_send = 2;
  document_agree = 2;

  document_photo_loading = false;
  document_photo_show = false;
  document_photo_base64 = '';
  document_photo_file: any;

  list_tip_ope: any[];
  list_tipes: any[] = [
    {'id': '1', 'name': 'Caja Soles'},
    {'id': '2', 'name': 'Caja Dólares'},
    {'id': '3', 'name': 'Banco M/N Cta Cte'},
    {'id': '4', 'name': 'Banco M/E Cta Cte'}
  ];

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) {
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {

    this.isloading = true;
    this.isSaved = false;

    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1; // January is 0!
    const yyyy = today.getFullYear();

    this.isDevolucion = false;
    this.isTransferencia = false;
    this.isNegative = false;

    this.debehaber = 0;

    this.date1 = { date: { year: yyyy, month: mm, day: dd } };

    this.list_tip_docs = JSON.parse(localStorage.getItem('document_types'));
    this.list_categories = JSON.parse(localStorage.getItem('categories'));
    this.list_tip_ope = JSON.parse(localStorage.getItem('operation_types'));

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.list_trips = JSON.parse(aux).trips;
          this.list_providers = JSON.parse(aux).providers;

          for (let xx of this.list_trips) {
            this.options.push(xx.code);
          }

          for (let xx of this.list_providers) {
            this.options2.push(xx.name);
          }

          this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter(val))
          );

        this.filteredOptions2 = this.myControl2.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

      this.showImg = false;

      // 1 - true // 2 - false
      this.document_send = 2;
      this.document_agree = 2;

      this.register = JSON.parse(localStorage.getItem('edittrip'));
      if (JSON.parse(localStorage.getItem('edittrip')).operation_type.indexOf('Devoluci') !== -1) {
        this.isDevolucion = true;
      }

      if (JSON.parse(localStorage.getItem('edittrip')).operation_type.indexOf('Transfe') !== -1) {
        this.isTransferencia = true;
      }
      const zz = this.register.date;
      this.date1 = { date: { year: Number(zz.split('-')[0]), month: Number(zz.split('-')[1]), day: Number(zz.split('-')[2]) } };
      this.myControl.setValue(this.register.trip);
      this.myControl2.setValue(this.register.provider);
      this.register.type = this.parseType(this.register.type);
      this.register.operation_type = this.parseOperation(this.register.operation_type);
      this.register.exchange_rate = this.register.exchange_rate === 'None' ? '' : this.register.exchange_rate;

      if (Number(this.register.amount) < 0 ) {
        this.isNegative = true;
        this.register.amount = Number(Number(this.register.amount) * -1).toFixed(2);
      }

      /*this.register = {
        id: 0,
        trip: '',
        date: '',
        operation_type: 0,
        category: 0,
        provider: '',
        description: '',
        cheque_number: '',
        exchange_rate: '',
        type: '0',
        amount: '',
        currency: 'S'
      };*/

      //console.log(localStorage.getItem('edittrip'));
  }

  parseOperation(x) {
    for (const xx of this.list_tip_ope) {
      if (xx.name === x) {
        return xx.id;
      }
    }
  }

  goback() {
    this.location.back();
  }

  parseType(x) {
    const list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for (const xx of list_types) {
      if (xx.name === x) {
        return xx.id;
      }
    }
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }
  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  montoChange() {
    this.register.amount = Number(this.register.amount).toFixed(2);
  }

  numberChange(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode === 46) {

    } else {
      if (event.keyCode === 32 || event.keyCode === 8 || !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }

  canDeactivate() {
    console.log('i am navigating away');
    if(this.isSaved == false){
      return window.confirm('¿Realmente Desea Salir?');
    }
    return true;
  }

  toggleCurrency(){
    if(this.register.currency == 'S'){
      this.register.currency = 'D';
    } else {
      this.register.currency = 'S';
    }
  }

}
