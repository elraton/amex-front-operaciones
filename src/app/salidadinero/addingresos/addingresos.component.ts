import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { Trip } from '../../common/trip';
import { Dinero } from '../../common/dinero';
import {IMyDpOptions} from 'mydatepicker';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  selector: 'app-add-ingresos',
  templateUrl: './addingresos.component.html',
  styleUrls: ['./addingresos.component.css']
})


export class AddIngresosComponent implements OnInit {

  myControl: FormControl = new FormControl();
  myControl2: FormControl = new FormControl();

  options = [];
  options2 = [];

  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;

  list_trips: any[];
  list_providers: any[];
  list_tip_docs: any[];
  list_categories: any[];

  TripListStrings: string[];
  tripText: any;

  register: Dinero;

  photo_base_64: string;
  photo_file: File;
  showImg: boolean;

  debehaber: number;

  isloading: boolean;
  isSaved: boolean;

  baseimg2: string = localStorage.getItem('baseassets');
  
  isUploading: boolean = false;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px',
  };



  public date1: any;
  public date2: any;

  document_send = 2;
  document_agree = 2;

  ruc_aux = '';
  razon_aux = '';
  showproviderinfo = false;

  document_photo_loading = false;
  document_photo_show = false;
  document_photo_base64 = '';
  document_photo_file: any;

  list_tip_ope: any[];
  list_tipes: any[] = [
    {'id': '1', 'name': 'Caja Soles'},
    {'id': '2', 'name': 'Caja Dólares'},
    {'id': '3', 'name': 'Banco M/N Cta Cte'},
    {'id': '4', 'name': 'Banco M/E Cta Cte'}
  ];

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) {
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {

    this.isloading = true;
    this.isSaved = false;

    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth()+1; //January is 0!
    const yyyy = today.getFullYear();

    this.debehaber = 0;

    this.date1 = { date: { year: yyyy, month: mm, day: dd } };

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          localStorage.setItem('trip_types', JSON.stringify(JSON.parse(aux).trip_types));
          localStorage.setItem('operation_types', JSON.stringify(JSON.parse(aux).operation_types));
          localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
          localStorage.setItem('document_types', JSON.stringify(JSON.parse(aux).document_types));

          this.list_tip_docs = JSON.parse(localStorage.getItem('document_types'));
          this.list_categories = JSON.parse(localStorage.getItem('categories'));
          this.list_tip_ope = JSON.parse(localStorage.getItem('operation_types'));

          this.list_tip_ope = this.list_tip_ope.filter(
            op => op.name.toLowerCase().indexOf('otros') !== -1 || op.name.toLowerCase().indexOf('ingres') !== -1
          );
        },
        error => {
          console.log(error.error.text);
        }
      );

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.list_trips = JSON.parse(aux).trips;
          this.list_providers = JSON.parse(aux).providers;

          for(let xx of this.list_trips) {
            this.options.push(xx.code);
          }

          for(let xx of this.list_providers) {
            this.options2.push(xx.name);
          }

          this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter(val))
          );

        this.filteredOptions2 = this.myControl2.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

      this.showImg = false;

      //1 - true // 2 - false
      this.document_send = 2;
      this.document_agree = 2;

      this.register = {
        id: 0,
        trip: '',
        date: '',
        operation_type: 0,
        category: 0,
        provider: '',
        description: '',
        cheque_number: '',
        exchange_rate: '',
        type: '0',
        amount: '',
        currency: 'S'
      };
  }

  toggleCurrency(){
    if(this.register.currency == 'S'){
      this.register.currency = 'D';
    } else {
      this.register.currency = 'S';
    }
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }
  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  exchangeChange() {
    if ( Number(this.register.exchange_rate) > 0) {
      this.register.exchange_rate = Number(this.register.exchange_rate).toFixed(2);
    }
  }
  montoChange() {
    if ( Number(this.register.amount) > 0 ) {
      this.register.amount = Number(this.register.amount).toFixed(2);
    }
  }

  numberChange(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode === 46) {

    } else {
      if (event.keyCode === 32 || event.keyCode === 8 || !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }

  canDeactivate() {
    console.log('i am navigating away');
    if(this.isSaved == false){
      return window.confirm('¿Realmente Desea Salir?');
    }
    return true;
  }

  ChangeTipoCaja() {
    if (this.register.type === '1' || this.register.type === '3') {
      this.register.currency = 'S';
    }
    if (this.register.type === '2' || this.register.type === '4') {
      this.register.currency = 'D';
    }
  }

  goback() {
    this.location.back();
  }

  providerChange(yy) {
    /* buscar proveedor y luego asignarle la categoria */
    this.showproviderinfo = false;
    for (const xx of this.list_providers) {
      if (xx.name === this.myControl2.value) {
        this.ruc_aux = xx.ruc;
        if ( xx.socialreason !== 'undefined' && xx.socialreason) {
          this.razon_aux = xx.socialreason;
        }
        this.showproviderinfo = true;
      }
    }
  }

  cleanForm() {
    this.register = {
      id: 0,
      trip: '',
      date: '',
      operation_type: 0,
      category: 0,
      provider: '',
      description: '',
      cheque_number: '',
      exchange_rate: '',
      type: '0',
      amount: '',
      currency: 'S'
    };

    /*const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();

    this.date1 = { date: { year: yyyy, month: mm, day: dd } };*/

    this.isSaved = false;

    this.myControl.setValue('');
    this.myControl2.setValue('');
  }

  save(opt: Number) {

    let error = false;

    this.register.provider = '';
    if(this.myControl2.value !== null || this.myControl2.value !== ''){
      for(let xx of this.list_providers){
        if (xx.name === this.myControl2.value) {
          this.register.provider = xx.id;
        }
      }
    }

    if (this.register.provider === '') {
      this.toastyService.warning('No existe el proveedor');
      error = true;
    }

    if (this.register.description === '') {
      this.toastyService.warning('Debe ingresar una descripción');
      error = true;
    }

    if (this.register.operation_type === 0) {
      this.toastyService.warning('Ingrese un Tipo de Operación');
      error = true;
    }

    if (this.register.type === '0') {
      this.toastyService.warning('Ingrese un Tipo de Caja');
      error = true;
    }

    if (this.date1.formatted !== undefined) {
      // this.register.date = this.date1.formatted;
      const dat = this.date1.formatted.split('/');
      this.register.date = dat[2] + '-' + dat[1] + '-' + dat[0];
    } else {
      this.register.date = this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day;
    }

    if(Number(this.register.amount) <= 0){
      this.toastyService.warning('Ingrese un monto valido');
      error = true;
    }

    if(error === false) {
      let formData = new FormData();
      let hour = new Date().getHours();
      let min = new Date().getMinutes();
      let sec = new Date().getSeconds();

      formData.append('trip', this.register.trip);
      formData.append('date', this.register.date+'T'+hour+':'+min+':'+sec);
      formData.append('operation_type', this.register.operation_type.toString());
      formData.append('category', this.register.category.toString());
      formData.append('provider', this.register.provider);
      formData.append('description', this.register.description);
      formData.append('cheque_number', this.register.cheque_number);
      formData.append('exchange_rate', this.register.exchange_rate);
      formData.append('type', this.register.type);
      formData.append('amount', Number(this.register.amount).toFixed(2));
      formData.append('currency', this.register.currency);

      this.http.post(localStorage.getItem('baseurl') + 'list_outlays/add/', formData)
        .subscribe(
          res => {
            console.log(res);
            this.toastyService.success('Devolución creada');
              this.isSaved = true;
              if (opt === 1) {
                setTimeout(() => {
                  this.location.back();
                }, 1000);
              } else {
                this.cleanForm();
              }
          },
          error2 => {
            if (error2.error.message === 'Register not edited, the cash register is closed') {
              this.toastyService.error('La Caja ya esta cerrada');
            } else {
              this.toastyService.error('Ocurrio un error');
            }
            console.log(error2);
          }
        );
    }
  }

  cancelar() {
    this.location.back();
  }

}
