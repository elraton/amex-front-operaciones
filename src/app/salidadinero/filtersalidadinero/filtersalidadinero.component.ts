import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Page } from '../../common/page';
import { Dinero } from '../../common/dinero';
import {IMyDpOptions} from 'mydatepicker';
import {MatSort, MatTableDataSource} from '@angular/material';
import {RegistroAux} from '../../common/registroaux';
import {SelectionModel} from '@angular/cdk/collections';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { PreviousRouteService } from '../previousrouteservice';

@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './filtersalidadinero.component.html',
  styleUrls: ['./filtersalidadinero.component.css']
})
export class FilterSalidaDineroComponent implements OnInit {

  displayedColumns = [
    'select',
    'trip',
    'date',
    'operation_type',
    'category',
    'provider',
    'cheque_number',
    'exchange_rate',
    'type',
    'amount',
    'aux'
  ];

  countSoles = 0;
  countDolares = 0;
  countBcoSoles = 0;
  countBcoDolares = 0;

  // Caja Soles
  dataSource_soles;
  CajaSoles;
  @ViewChild(MatSort) sort_soles: MatSort;
  selection_soles;

  baseimg2 = localStorage.getItem('baseassets');

  // Caja Dolares
  dataSource_dolares;
  CajaDolares;
  @ViewChild(MatSort) sort_dolares: MatSort;
  selection_dolares;

  // Banco Soles
  dataSource_bco_soles;
  BancoSoles;
  @ViewChild(MatSort) sort_bco_soles: MatSort;
  selection_bco_soles;

  // Banco Dolares
  dataSource_bco_dolares;
  BancoDolares;
  @ViewChild(MatSort) sort_bco_dolares: MatSort;
  selection_bco_dolares;

  export_message = 'Exportar Todo';

  loadingbcodolares = true;
  loadingbcosoles = true;
  loadingdolares = true;
  loadingsoles = true;

  showBancoDolares = false;
  showBancoSoles = false;
  showDolares = false;
  showSoles = false;

  isLoadingResults: boolean;

  proveedores_list: any[];
  categoria_list: any[];
  list_trips: any[];

  showNotFound: string;

  disablePrev_soles: string;
  disableNext_soles: string;

  disablePrev_dolares: string;
  disableNext_dolares: string;

  disablePrev_bco_soles: string;
  disableNext_bco_soles: string;

  disablePrev_bco_dolares: string;
  disableNext_bco_dolares: string;

  filterSelect: number;

  page_soles: number;
  totalpages_soles: number;

  page_dolares: number;
  totalpages_dolares: number;

  page_bco_soles: number;
  totalpages_bco_soles: number;

  page_bco_dolares: number;
  totalpages_bco_dolares: number;

  paginator_soles: any[];
  paginator_dolares: any[];
  paginator_bco_soles: any[];
  paginator_bco_dolares: any[];

  fecha: string;
  proveedores: string;
  categoria: string;
  viaje: string;
  moneda: string;

  proveedoresv: string;
  categoriav: string;
  tripv: string;
  monedav: string;
  isExporting = false;

  rol = JSON.parse(localStorage.getItem('rol'));
  showadd = this.rol.mov_add;
  showview = this.rol.mov_view;
  showedit = this.rol.mov_edit;
  showdelete = this.rol.mov_delete;
  showexport = this.rol.mov_export;

  fechasFilter = 0;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptions2: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;

  current: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private previousRouteService: PreviousRouteService
    ) { }

  ngOnInit() {

    this.isLoadingResults = true;

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.proveedores_list = JSON.parse(aux).providers;
          this.list_trips = JSON.parse(aux).trips;
        },
        error => {
          console.log(error.error.text);
        }
      );


    this.categoria_list = JSON.parse(localStorage.getItem('categories'));

    this.disablePrev_soles = 'disabled';
    this.disableNext_soles = 'disabled';

    this.disablePrev_dolares = 'disabled';
    this.disableNext_dolares = 'disabled';

    this.disablePrev_bco_soles = 'disabled';
    this.disableNext_bco_soles = 'disabled';

    this.disablePrev_bco_dolares = 'disabled';
    this.disableNext_bco_dolares = 'disabled';

    this.page_soles = 1;
    this.totalpages_soles = 1;

    this.page_dolares = 1;
    this.totalpages_dolares = 1;

    this.page_bco_soles = 1;
    this.totalpages_bco_soles = 1;

    this.page_bco_dolares = 1;
    this.totalpages_bco_dolares = 1;

    this.filterSelect = 0;

    this.fecha = 'no-display';
    this.proveedores = 'no-display';
    this.categoria = 'no-display';
    this.viaje = 'no-display';
    this.moneda = 'no-display';

    this.current = Number(localStorage.getItem('currentCashFilter'));

    this.proveedoresv = '0';
    this.categoriav = '0';
    this.tripv = '0';


    this.route.params.subscribe(params => {
      this.page_soles = +params['page'];
      this.page_dolares = +params['page'];
      this.page_bco_soles = +params['page'];
      this.page_bco_dolares = +params['page'];

      this.filterSelect = params['type'];
      switch (+this.filterSelect) {
        case 1: {
          this.date1 = {
            date: {
              year: Number(params['par1'].split('-')[2]),
              month: Number(params['par1'].split('-')[1]),
              day: Number(params['par1'].split('-')[0])
            }
          };
          this.date2 = {
            date: {
              year: Number(params['par2'].split('-')[2]),
              month: Number(params['par2'].split('-')[1]),
              day: Number(params['par2'].split('-')[0])
            }
          };
          this.fecha = '';
          break;
        }
        case 2: {
          this.proveedoresv = params['par1'];
          this.proveedores = '';
          break;
        }
        case 3: {
          this.categoriav = params['par1'];
          this.categoria = '';
          break;
        }
        case 4: {
          this.tripv = params['par1'];
          this.viaje = '';
          break;
        }
        case 5: {
          this.monedav = params['par1'];
          this.moneda = '';
          break;
        }
      }
    });

    this.CajaSoles = [];
    this.dataSource_soles = new MatTableDataSource<Dinero>(this.CajaSoles);
    this.selection_soles = new SelectionModel<Dinero>(true, []);
    this.dataSource_soles.sort = this.sort_soles;

    this.CajaDolares = [];
    this.dataSource_dolares = new MatTableDataSource<Dinero>(this.CajaDolares);
    this.selection_dolares = new SelectionModel<Dinero>(true, []);
    this.dataSource_dolares.sort = this.sort_dolares;

    this.BancoSoles = [];
    this.dataSource_bco_soles = new MatTableDataSource<Dinero>(this.BancoSoles);
    this.selection_bco_soles = new SelectionModel<Dinero>(true, []);
    this.dataSource_bco_soles.sort = this.sort_bco_soles;

    this.BancoDolares = [];
    this.dataSource_bco_dolares = new MatTableDataSource<Dinero>(this.BancoDolares);
    this.selection_bco_dolares = new SelectionModel<Dinero>(true, []);
    this.dataSource_bco_dolares.sort = this.sort_bco_dolares;

    this.RequestCajaByType('1', 1);
    this.RequestCajaByType('2', 1);
    this.RequestCajaByType('3', 1);
    this.RequestCajaByType('4', 1);

  }

  tabChanged(x: any) {
    this.current = x.index;
    if (this.current === 0) {
      const a = this.location.path().split('/');
      a[a.length - 1] = this.page_soles.toString();
      this.location.replaceState(a.join('/'));
    }
    if (this.current === 1) {
      const a = this.location.path().split('/');
      a[a.length - 1] = this.page_dolares.toString();
      this.location.replaceState(a.join('/'));
    }
    if (this.current === 2) {
      const a = this.location.path().split('/');
      a[a.length - 1] = this.page_bco_soles.toString();
      this.location.replaceState(a.join('/'));
    }
    if (this.current === 3) {
      const a = this.location.path().split('/');
      a[a.length - 1] = this.page_bco_dolares.toString();
      this.location.replaceState(a.join('/'));
    }
  }

  setTimeDisable(event) {
    if (event.formatted === '') {
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: 0, month: 0, day: 0}
      };
    } else {
      const aa = event.formatted.split('-');
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: Number(aa[0]), month: Number(aa[1]), day: Number(aa[2])}
      };
    }
  }

  RequestCajaByType(type, page) {
    this.isLoadingResults = true;
    const data = new FormData();
    data.append('type_cash', type);
    data.append('type', this.filterSelect.toString());

    switch (+this.filterSelect) {
      case 1: {
        data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
        data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
        break;
      }
      case 2: {
        data.append('name', this.proveedoresv);
        break;
      }
      case 3: {
        data.append('name', this.categoriav);
        break;
      }
      case 4: {
        data.append('trip', this.tripv);
        break;
      }
      case 5: {
        data.append('currency', this.monedav);
        break;
      }
    }
    this.http.post(localStorage.getItem('baseurl') + 'list_outlays/filter/?page=' + page, data)
      .subscribe(
        res => {
          const aux = JSON.stringify(res);

          switch (type) {
            case '1': {
              this.loadingsoles = false;
              this.CajaSoles = JSON.parse(aux).objects;
              if (this.CajaSoles.length > 0) {
                this.showSoles = true;
              }
              this.countSoles = JSON.parse(aux).items;
              this.dataSource_soles = new MatTableDataSource<Dinero>(this.CajaSoles);
              this.selection_soles = new SelectionModel<Dinero>(true, []);
              this.dataSource_soles.sort = this.sort_soles;
              this.totalpages_soles = JSON.parse(aux).num_pages;
              this.paginator_soles = [];
              for (let i = 0; i < this.totalpages_soles; i++) {
                if ( i + 1 === this.page_soles) {
                  this.paginator_soles.push({page: i + 1, state: 'active'});
                } else {
                  this.paginator_soles.push({page: i + 1, state: ''});
                }
              }
              break;
            }
            case '2': {
              this.loadingdolares = false;
              this.CajaDolares = JSON.parse(aux).objects;
              if (this.CajaDolares.length > 0) {
                this.showDolares = true;
              }
              this.countDolares = JSON.parse(aux).items;
              this.dataSource_dolares = new MatTableDataSource<Dinero>(this.CajaDolares);
              this.selection_dolares = new SelectionModel<Dinero>(true, []);
              this.dataSource_dolares.sort = this.sort_dolares;
              this.totalpages_dolares = JSON.parse(aux).num_pages;
              this.paginator_dolares = [];
              for (let i = 0; i < this.totalpages_dolares; i++) {
                if ( i + 1 === this.page_dolares) {
                  this.paginator_dolares.push({page: i + 1, state: 'active'});
                } else {
                  this.paginator_dolares.push({page: i + 1, state: ''});
                }
              }
              break;
            }
            case '3': {
              this.loadingbcosoles = false;
              this.BancoSoles = JSON.parse(aux).objects;
              if (this.BancoSoles.length > 0) {
                this.showBancoSoles = true;
              }
              this.countBcoSoles = JSON.parse(aux).items;
              this.dataSource_bco_soles = new MatTableDataSource<Dinero>(this.BancoSoles);
              this.selection_bco_soles = new SelectionModel<Dinero>(true, []);
              this.dataSource_bco_soles.sort = this.sort_bco_soles;
              this.totalpages_bco_soles = JSON.parse(aux).num_pages;
              this.paginator_bco_soles = [];
              for (let i = 0; i < this.totalpages_bco_soles; i++) {
                if ( i + 1 === this.page_bco_soles) {
                  this.paginator_bco_soles.push({page: i + 1, state: 'active'});
                } else {
                  this.paginator_bco_soles.push({page: i + 1, state: ''});
                }
              }
              break;
            }
            case '4': {
              this.loadingbcodolares = false;
              this.BancoDolares = JSON.parse(aux).objects;
              if (this.BancoDolares.length > 0) {
                this.showBancoDolares = true;
              }
              this.countBcoDolares = JSON.parse(aux).items;
              this.dataSource_bco_dolares = new MatTableDataSource<Dinero>(this.BancoDolares);
              this.selection_bco_dolares = new SelectionModel<Dinero>(true, []);
              this.dataSource_bco_dolares.sort = this.sort_bco_dolares;
              this.totalpages_bco_dolares = JSON.parse(aux).num_pages;
              this.paginator_bco_dolares = [];
              for (let i = 0; i < this.totalpages_bco_dolares; i++) {
                if ( i + 1 === this.page_bco_dolares) {
                  this.paginator_bco_dolares.push({page: i + 1, state: 'active'});
                } else {
                  this.paginator_bco_dolares.push({page: i + 1, state: ''});
                }
              }
              break;
            }
            default: {
              this.loadingbcodolares = false;
              this.loadingbcosoles = false;
              this.loadingdolares = false;
              this.loadingsoles = false;
              break;
            }
          }

          this.isLoadingResults = false;
        },
        error => {
          console.log(localStorage.getItem('baseurl'));
          console.log(error.error.text);
        }
    );
  }

  FilterDebe(x) {
    if (Number(x) > 0) {
      return x;
    } else {
      return '0.00';
    }
  }

  fechas() {
    if (this.fechasFilter !== 0) {
      switch (+this.fechasFilter) {
        case 1: {
          const today = new Date();
          this.date1 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date2 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 2: {
          const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
          this.date2 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 3: {
          const today = new Date();
          const week = today.getDay();
          const monday = week * 24 * 60 * 60 * 1000;
          const saturday = (6 - week) * 24 * 60 * 60 * 1000;
          const mon_date = new Date(today.getTime() - monday);
          const sat_date = new Date(today.getTime() + saturday);
          this.date1 = { date: { year: mon_date.getFullYear(), month: mon_date.getMonth() + 1, day: mon_date.getDate() } };
          this.date2 = { date: { year: sat_date.getFullYear(), month: sat_date.getMonth() + 1, day: sat_date.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 5: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), 0, 1);
          const last_day = new Date(today.getFullYear(), 11, 31);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  FilterHaber(x) {
    if (Number(x) < 0) {
      return Number(Number(x) * -1).toFixed(2);
    } else {
      return '0.00';
    }
  }

  stripDate(str: string) {
    return str.split('T')[0];
  }

  arrayObjToCsv(ar, filename) {
    let contenido = '';

    for (let i = 0; i < ar.length; i++) {
      if (i === 0) {
        contenido += Object.keys(ar[i]).join(',') + '\n';
      }
      contenido += Object.keys(ar[i]).map(function(key) {
        return ar[i][key];
      }).join(',') + '\n';
    }

    let blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
    let reader: any = new FileReader();
    reader.onload = function (evt) {

      const dwldLink = document.createElement('a');
      const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
      if (isSafariBrowser) {
        dwldLink.setAttribute('target', '_blank');
      }
      dwldLink.setAttribute('href', evt.target.result);
      dwldLink.setAttribute('download', filename + '.csv');
      dwldLink.style.visibility = 'hidden';
      document.body.appendChild(dwldLink);
      dwldLink.click();
      document.body.removeChild(dwldLink);
    };
    reader.readAsDataURL(blob);
  }

  export() {
    this.isExporting = true;
    switch (this.current) {
      case 0: {
        if (this.selection_soles.hasValue()) {
          let objs = [];
          for (const xx of this.selection_soles.selected) {
            objs.push(xx.id);
          }
          const data = new FormData();
          data.append('outlays', objs.join(','));
          data.append('type', '6');
          this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
          .subscribe(
            res => {
              console.log(res);
            },
            error => {
              console.log(error);
              if (error.error.text.indexOf('Caja') !== -1) {
                const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
                const dwldLink = document.createElement('a');
                const url = URL.createObjectURL(blob);
                const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
                if (isSafariBrowser) {
                  dwldLink.setAttribute('target', '_blank');
                }
                dwldLink.setAttribute('href', url);
                dwldLink.setAttribute('download', 'Desembolsos_CajaSoles_custom.csv');
                dwldLink.style.visibility = 'hidden';
                document.body.appendChild(dwldLink);
                dwldLink.click();
                document.body.removeChild(dwldLink);
                this.isExporting = false;
              }
            }
          );
          //this.arrayObjToCsv(objs, 'Desembolsos_Caja_Soles');
        } else {
          // es todo
        }
        break;
      }
      case 1: {
        if (this.selection_dolares.hasValue()) {
          const objs = [];
          for (const xx of this.selection_dolares.selected) {
            objs.push(xx.id);
          }
          const data = new FormData();
          data.append('outlays', objs.join(','));
          data.append('type', '6');
          this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
          .subscribe(
            res => {
              console.log(res);
            },
            error => {
              console.log(error);
              if (error.error.text.indexOf('Caja') !== -1) {
                const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
                const dwldLink = document.createElement('a');
                const url = URL.createObjectURL(blob);
                const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
                if (isSafariBrowser) {
                  dwldLink.setAttribute('target', '_blank');
                }
                dwldLink.setAttribute('href', url);
                dwldLink.setAttribute('download', 'Desembolsos_CajaDolares_custom.csv');
                dwldLink.style.visibility = 'hidden';
                document.body.appendChild(dwldLink);
                dwldLink.click();
                document.body.removeChild(dwldLink);
                this.isExporting = false;
              }
            }
          );
        } else {
          // exportar todo
        }
        break;
      }
      case 2: {
        if (this.selection_bco_soles.hasValue()) {
          const objs = [];
          for (const xx of this.selection_bco_soles.selected) {
            objs.push(xx.id);
          }
          const data = new FormData();
          data.append('outlays', objs.join(','));
          data.append('type', '6');
          this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
          .subscribe(
            res => {
              console.log(res);
            },
            error => {
              console.log(error);
              if (error.error.text.indexOf('Caja') !== -1) {
                const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
                const dwldLink = document.createElement('a');
                const url = URL.createObjectURL(blob);
                const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
                if (isSafariBrowser) {
                  dwldLink.setAttribute('target', '_blank');
                }
                dwldLink.setAttribute('href', url);
                dwldLink.setAttribute('download', 'Desembolsos_BancoSoles_custom.csv');
                dwldLink.style.visibility = 'hidden';
                document.body.appendChild(dwldLink);
                dwldLink.click();
                document.body.removeChild(dwldLink);
                this.isExporting = false;
              }
            }
          );
        } else {
          // exportar todo
        }
        break;
      }
      case 3: {
        if (this.selection_bco_dolares.hasValue()) {
          const objs = [];
          for (const xx of this.selection_bco_dolares.selected) {
            objs.push(xx.id);
          }
          const data = new FormData();
          data.append('outlays', objs.join(','));
          data.append('type', '6');
          this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
          .subscribe(
            res => {
              console.log(res);
            },
            error => {
              console.log(error);
              if (error.error.text.indexOf('Caja') !== -1) {
                const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
                const dwldLink = document.createElement('a');
                const url = URL.createObjectURL(blob);
                const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
                if (isSafariBrowser) {
                  dwldLink.setAttribute('target', '_blank');
                }
                dwldLink.setAttribute('href', url);
                dwldLink.setAttribute('download', 'Desembolsos_BancoDolares_custom.csv');
                dwldLink.style.visibility = 'hidden';
                document.body.appendChild(dwldLink);
                dwldLink.click();
                document.body.removeChild(dwldLink);
                this.isExporting = false;
              }
            }
          );
        } else {
          // exportar todo
        }
        break;
      }
      default:
        break;
    }

    if (this.export_message !== 'Exportar Seleccionados') {
      let data = new FormData();
      data.append('type_cash', '1');
      data.append('type', this.filterSelect.toString());
      switch (+this.filterSelect) {
        case 1: {
          data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
          data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
          break;
        }
        case 2: {
          data.append('name', this.proveedoresv);
          break;
        }
        case 3: {
          data.append('name', this.categoriav);
          break;
        }
        case 4: {
          data.append('trip', this.tripv);
          break;
        }
        case 5: {
          data.append('currency', this.monedav);
          break;
        }
      }
      this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'Desembolsos_CajaSoles.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
            this.isExporting = false;
          }
        }
      );


      data = new FormData();
      data.append('type_cash', '2');
      data.append('type', this.filterSelect.toString());
      switch (+this.filterSelect) {
        case 1: {
          data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
          data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
          break;
        }
        case 2: {
          data.append('name', this.proveedoresv);
          break;
        }
        case 3: {
          data.append('name', this.categoriav);
          break;
        }
        case 4: {
          data.append('trip', this.tripv);
          break;
        }
        case 5: {
          data.append('currency', this.monedav);
          break;
        }
      }
      this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'Desembolsos_CajaDolares.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
            this.isExporting = false;
          }
        }
      );

      data = new FormData();
      data.append('type_cash', '3');
      data.append('type', this.filterSelect.toString());
      switch (+this.filterSelect) {
        case 1: {
          data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
          data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
          break;
        }
        case 2: {
          data.append('name', this.proveedoresv);
          break;
        }
        case 3: {
          data.append('name', this.categoriav);
          break;
        }
        case 4: {
          data.append('trip', this.tripv);
          break;
        }
        case 5: {
          data.append('currency', this.monedav);
          break;
        }
      }
      this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'Desembolsos_Banco_MN.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
            this.isExporting = false;
          }
        }
      );

      data = new FormData();
      data.append('type_cash', '4');
      data.append('type', this.filterSelect.toString());
      switch (+this.filterSelect) {
        case 1: {
          data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
          data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
          break;
        }
        case 2: {
          data.append('name', this.proveedoresv);
          break;
        }
        case 3: {
          data.append('name', this.categoriav);
          break;
        }
        case 4: {
          data.append('trip', this.tripv);
          break;
        }
        case 5: {
          data.append('currency', this.monedav);
          break;
        }
      }
      this.http.post(localStorage.getItem('baseurl') + 'export_outlays/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'Desembolsos_Banco_ME.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
            this.isExporting = false;
          }
        }
      );
    }
  }


  selectChange(e: any, row: any, select: any) {
    e ? select.toggle(row) : null;

    if (select.hasValue()) {
      this.export_message = 'Exportar Seleccionados';
    } else {
      this.export_message = 'Exportar Todo';
    }
  }

   /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(select: any, datas) {
    const numSelected = select.selected.length;
    const numRows = datas.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(select, datas) {
    if (select.hasValue()) {
      this.export_message = 'Exportar Todo';
    } else {
      this.export_message = 'Exportar Seleccionados';
    }
    this.isAllSelected(select, datas) ?
      select.clear() :
      datas.data.forEach(row => select.select(row));
  }

  parseAmount(x) {
    if (Number(x) > 0 ) {
      return '+' + x;
    } else {
      return x;
    }
  }

  parseType(x) {
    const list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for (const xx of list_types) {
      if (xx.id === x) {
        return xx.name;
      }
    }
  }

  changePage(type, p) {
    this.isLoadingResults = true;
    switch (type) {
      case 1: {
        for (const i of this.paginator_soles) {
          i.state = '';
          if (i.page === p) {
            i.state = 'active';
          }
        }
        this.page_soles = p;
        if (this.page_soles === 1) {
          this.disablePrev_soles = 'disabled';
          this.disableNext_soles = 'disabled';
        }
        if (this.page_soles === this.totalpages_soles) {
          this.disableNext_soles = 'disabled';
        }
        if (this.page_soles > 1) {
          this.disablePrev_soles = '';
        }
        if (this.page_soles < this.totalpages_soles) {
          this.disableNext_soles = '';
        }

        const a = this.location.path().split('/');
        a[a.length - 1] = this.page_soles.toString();

        this.location.replaceState(a.join('/'));

        this.RequestCajaByType('1', this.page_soles);
        break;
      }
      case 2: {
        for (const i of this.paginator_dolares) {
          i.state = '';
          if (i.page === p) {
            i.state = 'active';
          }
        }
        this.page_dolares = p;
        if (this.page_dolares === 1) {
          this.disablePrev_dolares = 'disabled';
          this.disableNext_dolares = 'disabled';
        }
        if (this.page_dolares === this.totalpages_dolares) {
          this.disableNext_dolares = 'disabled';
        }
        if (this.page_dolares > 1) {
          this.disablePrev_dolares = '';
        }
        if (this.page_dolares < this.totalpages_dolares) {
          this.disableNext_dolares = '';
        }

        const a = this.location.path().split('/');
        a[a.length - 1] = this.page_dolares.toString();

        this.location.replaceState(a.join('/'));
        this.RequestCajaByType('2', this.page_dolares);
        break;
      }
      case 3: {
        for (const i of this.paginator_bco_soles) {
          i.state = '';
          if (i.page === p) {
            i.state = 'active';
          }
        }
        this.page_bco_soles = p;
        if (this.page_bco_soles === 1) {
          this.disablePrev_bco_soles = 'disabled';
          this.disableNext_bco_soles = 'disabled';
        }
        if (this.page_bco_soles === this.totalpages_bco_soles) {
          this.disableNext_bco_soles = 'disabled';
        }
        if (this.page_bco_soles > 1) {
          this.disablePrev_bco_soles = '';
        }
        if (this.page_bco_soles < this.totalpages_bco_soles) {
          this.disableNext_bco_soles = '';
        }

        const a = this.location.path().split('/');
        a[a.length - 1] = this.page_bco_soles.toString();

        this.location.replaceState(a.join('/'));
        this.RequestCajaByType('3', this.page_bco_soles);
        break;
      }
      case 4: {
        for (const i of this.paginator_bco_dolares) {
          i.state = '';
          if (i.page === p) {
            i.state = 'active';
          }
        }
        this.page_bco_dolares = p;
        if (this.page_bco_dolares === 1) {
          this.disablePrev_bco_dolares = 'disabled';
          this.disableNext_bco_dolares = 'disabled';
        }
        if (this.page_bco_dolares === this.totalpages_bco_dolares) {
          this.disableNext_bco_dolares = 'disabled';
        }
        if (this.page_bco_dolares > 1) {
          this.disablePrev_bco_dolares = '';
        }
        if (this.page_bco_dolares < this.totalpages_bco_dolares) {
          this.disableNext_bco_dolares = '';
        }

        const a = this.location.path().split('/');
        a[a.length - 1] = this.page_bco_dolares.toString();

        this.location.replaceState(a.join('/'));
        this.RequestCajaByType('4', this.page_bco_dolares);
        break;
      }
      default:
        break;
    }
  }

  editTrip(v) {
    if (this.showedit) {
      localStorage.setItem('edittrip', JSON.stringify(v));
      localStorage.setItem('currentCashFilter', this.current.toString());
      this.router.navigate(['/dinero/edit/' + v.id]);
      return;
    }
  }

  viewTrip(v) {
    localStorage.setItem('edittrip', JSON.stringify(v));
    localStorage.setItem('currentCashFilter', this.current.toString());
    this.router.navigate(['/dinero/view/' + v.id]);
    return;
  }

  deleteTrip(v, type) {
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Registro?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const varData = new FormData();
              varData.append('id', v.id);
              this.http.post(localStorage.getItem('baseurl') + 'list_outlays/delete/', varData)
                .subscribe(
                  res => {
                    console.log('200 ' + res);
                  },
                  error => {
                    console.log('error ' + error.error.text);
                    console.log(error.error.text === 'Outlay deleted.');
                    if (error.error.text === 'Outlay deleted.') {
                      resolve();
                      this.isLoadingResults = true;

                      switch (+type) {
                        case 1: {
                          this.CajaSoles = this.CajaSoles.filter(obj => obj !== v);
                          this.dataSource_soles = new MatTableDataSource<Dinero>(this.CajaSoles);
                          this.selection_soles = new SelectionModel<Dinero>(true, []);
                          this.dataSource_soles.sort = this.sort_soles;
                          this.toastyService.success('Registro Eliminado');
                          break;
                        }
                        case 2: {
                          this.CajaDolares = this.CajaDolares.filter(obj => obj !== v);
                          this.dataSource_dolares = new MatTableDataSource<Dinero>(this.CajaDolares);
                          this.selection_dolares = new SelectionModel<Dinero>(true, []);
                          this.dataSource_dolares.sort = this.sort_dolares;
                          this.toastyService.success('Registro Eliminado');
                          break;
                        }
                        case 3: {
                          this.BancoSoles = this.BancoSoles.filter(obj => obj !== v);
                          this.dataSource_bco_soles = new MatTableDataSource<Dinero>(this.BancoSoles);
                          this.selection_bco_soles = new SelectionModel<Dinero>(true, []);
                          this.dataSource_bco_soles.sort = this.sort_bco_soles;
                          this.toastyService.success('Registro Eliminado');
                          break;
                        }
                        case 4: {
                          this.BancoDolares = this.BancoDolares.filter(obj => obj !== v);
                          this.dataSource_bco_dolares = new MatTableDataSource<Dinero>(this.BancoDolares);
                          this.selection_bco_dolares = new SelectionModel<Dinero>(true, []);
                          this.dataSource_bco_dolares.sort = this.sort_bco_dolares;
                          this.toastyService.success('Registro Eliminado');
                          break;
                        }

                        default:
                          break;
                      }
                      this.isLoadingResults = false;
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }

  filtrar() {
    switch (+this.filterSelect) {
      case 1: {
        if (this.date1.formatted !== undefined && this.date2.formatted !== undefined) {
          this.RequestCajaByType('1', 1);
          this.RequestCajaByType('2', 1);
          this.RequestCajaByType('3', 1);
          this.RequestCajaByType('4', 1);
          this.location.replaceState('/dinero/filter/' +
          this.filterSelect + '/' +
          this.date1.formatted.split('/').join('-') + '/' +
          this.date2.formatted.split('/').join('-') + '/1');
          this.showBancoDolares = false;
          this.showBancoSoles = false;
          this.showSoles = false;
          this.showDolares = false;
          this.loadingbcodolares = true;
          this.loadingbcosoles = true;
          this.loadingdolares = true;
          this.loadingsoles = true;
        } else {
          // error de fechas
          if (this.date1 !== undefined && this.date2 !== undefined) {
            this.RequestCajaByType('1', 1);
            this.RequestCajaByType('2', 1);
            this.RequestCajaByType('3', 1);
            this.RequestCajaByType('4', 1);
            this.location.replaceState('/dinero/filter/' +
              this.filterSelect + '/' +
              this.date1.date.day + '-' + this.date1.date.month + '-' + this.date1.date.year + '/' +
              this.date2.date.day + '-' + this.date2.date.month + '-' + this.date2.date.year + '/1');
            this.showBancoDolares = false;
            this.showBancoSoles = false;
            this.showSoles = false;
            this.showDolares = false;
            this.loadingbcodolares = true;
            this.loadingbcosoles = true;
            this.loadingdolares = true;
            this.loadingsoles = true;
          } else {
            // mostrar error
            this.toastyService.warning('Debe ingresar 2 fechas validas');
          }
        }
        break;
      }
      case 2: {
        if (this.proveedoresv !== '0') {
          this.RequestCajaByType('1', 1);
          this.RequestCajaByType('2', 1);
          this.RequestCajaByType('3', 1);
          this.RequestCajaByType('4', 1);
          this.location.replaceState('/dinero/filter/' + this.filterSelect + '/' + this.proveedoresv + '/1');
          this.showBancoDolares = false;
          this.showBancoSoles = false;
          this.showSoles = false;
          this.showDolares = false;
          this.loadingbcodolares = true;
          this.loadingbcosoles = true;
          this.loadingdolares = true;
          this.loadingsoles = true;
        } else {
          // error de proveedor
          this.toastyService.warning('Debe Elegir un proveedor');
        }
        break;
      }
      case 3: {
        if (this.categoriav !== '0') {
          this.RequestCajaByType('1', 1);
          this.RequestCajaByType('2', 1);
          this.RequestCajaByType('3', 1);
          this.RequestCajaByType('4', 1);
          this.location.replaceState('/dinero/filter/' + this.filterSelect + '/' + this.categoriav + '/1');
          this.showBancoDolares = false;
          this.showBancoSoles = false;
          this.showSoles = false;
          this.showDolares = false;
          this.loadingbcodolares = true;
          this.loadingbcosoles = true;
          this.loadingdolares = true;
          this.loadingsoles = true;
        } else {
          // error de categoria
          this.toastyService.warning('Debe Elegir una categoria');
        }
        break;
      }
      case 4: {
        if (this.tripv !== '0') {
          this.RequestCajaByType('1', 1);
          this.RequestCajaByType('2', 1);
          this.RequestCajaByType('3', 1);
          this.RequestCajaByType('4', 1);
          this.location.replaceState('/dinero/filter/' + this.filterSelect + '/' + this.tripv + '/1');
          this.showBancoDolares = false;
          this.showBancoSoles = false;
          this.showSoles = false;
          this.showDolares = false;
          this.loadingbcodolares = true;
          this.loadingbcosoles = true;
          this.loadingdolares = true;
          this.loadingsoles = true;
        } else {
          // error de categoria
          this.toastyService.warning('Debe Elegir un viaje');
        }
        break;
      }
      case 5: {
        if (this.monedav !== '0') {
          this.RequestCajaByType('1', 1);
          this.RequestCajaByType('2', 1);
          this.RequestCajaByType('3', 1);
          this.RequestCajaByType('4', 1);
        } else {
          // error de categoria
        }
        break;
      }
      default:
        break;
    }
  }

  previusPage(type) {
    switch (type) {
      case 1: {
        this.changePage(1, this.page_soles - 1);
        break;
      }
      case 2: {
        this.changePage(2, this.page_dolares - 1);
        break;
      }
      case 3: {
        this.changePage(3, this.page_bco_soles - 1);
        break;
      }
      case 4: {
        this.changePage(4, this.page_bco_dolares - 1);
        break;
      }
      default:
        break;
    }
  }

  nextPage(type) {
    switch (type) {
      case 1: {
        this.changePage(1, this.page_soles + 1);
        break;
      }
      case 2: {
        this.changePage(2, this.page_dolares + 1);
        break;
      }
      case 3: {
        this.changePage(3, this.page_bco_soles + 1);
        break;
      }
      case 4: {
        this.changePage(4, this.page_bco_dolares + 1);
        break;
      }
      default:
        break;
    }
  }

  goback() {
    this.location.back();
  }

  filtrarForm(e, val) {
    switch (val) {
      case '0': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '1': {
        this.fecha = '';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '2': {
        this.fecha = 'no-display';
        this.proveedores = '';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '3': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = '';
        this.viaje = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '4': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = '';
        this.moneda = 'no-display';
        break;
      }
      case '5': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.moneda = '';
        break;
      }
      default:{
        break;
      }
    }
  }

}
