import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-with-social',
  templateUrl: './with-social.component.html'
})
export class WithSocialComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
  ) {

    this.toastyConfig.theme = 'bootstrap';
  }

  baseimg: string = localStorage.getItem('baseassets');

  username = '';
  password = '';
  system = 0;

  ngOnInit() {
    localStorage.setItem('token', '');
    localStorage.setItem('username', '');
  }

  login() {
    console.log('system: ' + this.system);
    const formData = new FormData();

    formData.append('username', this.username);
    formData.append('password', this.password);

    if (+this.system === 0) { // operaciones
      this.http.post(localStorage.getItem('baseurlope') + 'token_auth/', formData)
      .subscribe(
        res => {
          localStorage.setItem('token', JSON.parse(JSON.stringify(res)).token);
          localStorage.setItem('username', this.username);
          // this.router.navigate(['/operaciones/']);
          window.location.href = '/operaciones/';
        },
        error => {
          console.log(error);
          this.toastyService.error('Usuario o Contraseña incorrectos');
        }
      );
    } else { // administracion
      this.http.post(localStorage.getItem('baseurladm') + 'token_auth/', formData)
      .subscribe(
        res => {
          localStorage.setItem('token', JSON.parse(JSON.stringify(res)).token);
          localStorage.setItem('username', this.username);
          // this.router.navigate(['/administracion/']);
          window.location.href = '/administracion/';
        },
        error => {
          console.log(error);
          this.toastyService.error('Usuario o Contraseña incorrectos');
        }
      );
    }
  }

}
