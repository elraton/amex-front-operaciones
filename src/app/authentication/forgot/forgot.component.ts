import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html'
})
export class ForgotComponent implements OnInit {

  username: string;

  public form: FormGroup;
  baseimg: string = localStorage.getItem('baseassets');
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
  ) {
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  resetpassword() {
    const formData = new FormData();

    formData.append('username', this.username);

    this.http.post(localStorage.getItem('baseurl') + 'resetpassword/', formData)
      .subscribe(
        res => {
          this.toastyService.success('Se le envio un email con su nueva contraseña');
          setTimeout(() => {
            this.router.navigate(['/authentication/login']);
          }, 3000);
        },
        error => {
          this.toastyService.error('Usuario inexistente en el sistema');
        }
      );
  }

  onSubmit() {
    this.router.navigate ( [ '/' ] );
  }
}
