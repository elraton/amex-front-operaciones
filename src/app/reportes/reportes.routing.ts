import { Routes } from '@angular/router';

import { ReportesComponent } from './reportes.component';

import { AuthGuard } from '../guard';

export const ReportesRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ReportesComponent,
    data: {
      breadcrumb: 'Reportes',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: ':type/:provider',
    canActivate: [AuthGuard],
    component: ReportesComponent,
    data: {
      breadcrumb: 'Reportes',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: ':type',
    canActivate: [AuthGuard],
    component: ReportesComponent,
    data: {
      breadcrumb: 'Reportes',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

/*export const ReportesRoutes: Routes = [{
  path: '',
  component: ReportesComponent,
  data: {
    breadcrumb: 'Reportes',
    status: true
  },
  children: [
      {
        path: ':type/:provider',
          component: ReportesComponent,
          data: {
              breadcrumb: 'Reportes',
              status: false
          }
      }
    ]
}];*/