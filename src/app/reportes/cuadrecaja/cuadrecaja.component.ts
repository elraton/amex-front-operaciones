import { Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { CajaSoles } from '../../common/cajasoles';
import { IMyDpOptions } from 'mydatepicker';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import {Sort} from '@angular/material';


@Component({
  selector: 'app-cuadre-caja',
  templateUrl: './cuadrecaja.component.html',
  styleUrls: ['./cuadrecaja.component.css']
})
export class CuadreCajaComponent implements OnInit {

  baseimg2: string = localStorage.getItem('baseassets');

  displayedColumns = [
    'date',
    'trip',
    'provider',
    'description',
    'category',
    'operation_type',
    'amount',
    'exchange_rate',
    'cheque_number'
  ];

  countSoles = 0;
  countDolares = 0;
  countBcoSoles = 0;
  countBcoDolares = 0;

  dataSource;
  dataSourceDolares;
  dataSourceBancoSoles;
  dataSourceBancoDolares;

  CajaSoles: CajaSoles[];
  CajaDolares: CajaSoles[];
  CajaBancoSoles: CajaSoles[];
  CajaBancoDolares: CajaSoles[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) sortDolares: MatSort;
  @ViewChild(MatSort) sortBancoSoles: MatSort;
  @ViewChild(MatSort) sortBancoDolares: MatSort;

  selection;
  selectionDolares;
  selectionBancoSoles;
  selectionBancoDolares;

  isLoadingResults: boolean;
  showTables: boolean;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptions2: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;


  cajaSolesIni: string;
  cajaSolesFin: string;

  cajaDolaresIni: string;
  cajaDolaresFin: string;

  cajaBancoSolesIni: string;
  cajaBancoSolesFin: string;

  cajaBancoDolaresIni: string;
  cajaBancoDolaresFin: string;

  SumDebeSoles1 = 0;
  SumHaberSoles1 = 0;

  SumDebeDolares = 0;
  SumHaberDolares = 0;

  SumDebeBancoSoles = 0;
  SumHaberBancoSoles = 0;

  SumDebeBancoDolares = 0;
  SumHaberBancoDolares = 0;

  current = 0;

  activeExport = 'disabled';

  showSoles = false;
  showDolares = false;
  showBancoSoles = false;
  showBancoDolares = false;

  loadingbcodolares = true;
  loadingbcosoles = true;

  loadingsoles = true;
  loadingdolares = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) {  }

  ngOnInit() {

    this.showTables = false;

  }

  tabChanged(x: any) {
    this.current = x.index;
  }

  setTimeDisable(event) {
    if (event.formatted === '') {
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: 0, month: 0, day: 0}
      };
    } else {
      const aa = event.formatted.split('-');
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: Number(aa[0]), month: Number(aa[1]), day: Number(aa[2])}
      };
    }
  }

  cutText(ss: string) {
    return ss.slice(0, 7) + '...';
  }

  generar() {
    this.loadingsoles = true;
    this.loadingdolares = true;
    this.loadingbcosoles = true;
    this.loadingbcodolares = true;
    this.showSoles = false;
    this.showDolares = false;
    this.showBancoDolares = false;
    this.showBancoSoles = false;

    this.showTables = true;

    this.SumDebeSoles1 = 0;
    this.SumHaberSoles1 = 0;
    this.activeExport = 'disabled';
    let _date1 = '';
    let _date2 = '';
    if (this.date1 !== undefined && this.date1 !== null){
      if (this.date1.formatted !== undefined) {
        _date1 = this.date1.formatted;
      } else {
        _date1 = '';
      }
    } else {
      _date1 = '';
    }

    if (this.date2 !== undefined && this.date2 !== null){
      if (this.date2.formatted !== undefined) {
        _date2 = this.date2.formatted;
      } else {
        _date2 = '';
      }
    } else {
      _date2 = '';
    }

    if (_date1 !== '' && _date2 !== ''){
      let data = new FormData();
      data.append('date_start', this.date1.formatted.split('/')[2] +
        '-' + this.date1.formatted.split('/')[1] +
        '-' + this.date1.formatted.split('/')[0]);
      data.append('date_end', this.date2.formatted.split('/')[2] + '-' +
        this.date2.formatted.split('/')[1] + '-' +
        this.date2.formatted.split('/')[0]);
      data.append('type', '1');
      this.isLoadingResults = true;

      this.http.post(localStorage.getItem('baseurl') + 'report_cash/', data)
        .subscribe(
          res => {
            let aux = JSON.stringify(res);
            if (aux !== undefined) {
              this.showSoles = true;
            }
            this.CajaSoles = JSON.parse(aux).objs;
            this.countSoles = this.CajaSoles.length;
            this.cajaSolesIni = Number(JSON.parse(aux).init).toFixed(2);

            this.SumDebeSoles1 = this.CalculateDebe(this.CajaSoles);

            this.cajaSolesFin = (Number(this.cajaSolesIni) + Number(this.SumDebeSoles1)).toFixed(2);

            this.isLoadingResults = false;
            this.loadingsoles = false;

            // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSource = new MatTableDataSource<CajaSoles>(this.CajaSoles);
            this.selection = new SelectionModel<CajaSoles>(true, []);
            this.dataSource.sort = this.sort;
          },
          error => {
            console.log(error.error.text);
            this.loadingsoles = false;
          }
      );

      data = new FormData();
      data.append('date_start', this.date1.formatted.split('/')[2] +
        '-' + this.date1.formatted.split('/')[1] +
        '-' + this.date1.formatted.split('/')[0]);
      data.append('date_end', this.date2.formatted.split('/')[2] + '-' +
        this.date2.formatted.split('/')[1] + '-' +
        this.date2.formatted.split('/')[0]);
      data.append('type', '2');

      this.http.post(localStorage.getItem('baseurl') + 'report_cash/', data)
        .subscribe(
          res => {
            const aux = JSON.stringify(res);
            if (aux !== undefined) {
              this.showDolares = true;
            }
            console.log(aux);
            this.CajaDolares = JSON.parse(aux).objs;
            this.countDolares = this.CajaDolares.length;
            this.cajaDolaresIni = Number(JSON.parse(aux).init).toFixed(2);

            this.SumDebeDolares = this.CalculateDebe(this.CajaDolares);

            this.cajaDolaresFin = (Number(this.cajaDolaresIni) + Number(this.SumDebeDolares)).toFixed(2);

            this.isLoadingResults = false;
            this.loadingdolares = false;
            // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSourceDolares = new MatTableDataSource<CajaSoles>(this.CajaDolares);
            this.selectionDolares = new SelectionModel<CajaSoles>(true, []);
            this.dataSourceDolares.sort = this.sortDolares;
          },
          error => {
            console.log(error.error.text);
            this.loadingdolares = false;
          }
      );


      data = new FormData();
      data.append('date_start', this.date1.formatted.split('/')[2] +
        '-' + this.date1.formatted.split('/')[1] +
        '-' + this.date1.formatted.split('/')[0]);
      data.append('date_end', this.date2.formatted.split('/')[2] + '-' +
        this.date2.formatted.split('/')[1] + '-' +
        this.date2.formatted.split('/')[0]);
      data.append('type', '3');

      this.http.post(localStorage.getItem('baseurl') + 'report_cash/', data)
        .subscribe(
          res => {
            const aux = JSON.stringify(res);
            if (aux !== undefined) {
              this.showBancoSoles = true;
            }
            this.CajaBancoSoles = JSON.parse(aux).objs;
            this.countBcoSoles = this.CajaBancoSoles.length;
            this.cajaBancoSolesIni = Number(JSON.parse(aux).init).toFixed(2);
            this.SumDebeBancoSoles = this.CalculateDebe(this.CajaBancoSoles);

            this.cajaBancoSolesFin = (Number(this.cajaBancoSolesIni) + Number(this.SumDebeBancoSoles)).toFixed(2);

            this.isLoadingResults = false;
            this.loadingbcosoles = false;
            // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSourceBancoSoles = new MatTableDataSource<CajaSoles>(this.CajaBancoSoles);
            this.selectionBancoSoles = new SelectionModel<CajaSoles>(true, []);
            this.dataSourceBancoSoles.sort = this.sortBancoSoles;
          },
          error => {
            console.log(error.error.text);
            this.loadingbcosoles = false;
          }
      );

      data = new FormData();
      data.append('date_start', this.date1.formatted.split('/')[2] +
        '-' + this.date1.formatted.split('/')[1] +
        '-' + this.date1.formatted.split('/')[0]);
      data.append('date_end', this.date2.formatted.split('/')[2] + '-' +
        this.date2.formatted.split('/')[1] + '-' +
        this.date2.formatted.split('/')[0]);
      data.append('type', '4');

      this.http.post(localStorage.getItem('baseurl') + 'report_cash/', data)
        .subscribe(
          res => {
            const aux = JSON.stringify(res);
            if (aux !== undefined) {
              this.showBancoDolares = true;
            }
            this.CajaBancoDolares = JSON.parse(aux).objs;
            this.countBcoDolares = this.CajaBancoDolares.length;
            this.cajaBancoDolaresIni = Number(JSON.parse(aux).init).toFixed(2);

            this.SumDebeBancoDolares = 0;

            this.SumDebeBancoDolares = this.CalculateDebe(this.CajaBancoDolares);

            this.cajaBancoDolaresFin = (Number(this.cajaBancoDolaresIni) + Number(this.SumDebeBancoDolares)).toFixed(2);

            // this.cajaBancoDolaresFin = Number((Number(this.cajaBancoDolaresIni) + this.SumDebeBancoDolares) - this.SumHaberBancoDolares).toFixed(2);

            this.loadingbcodolares = false;

            this.isLoadingResults = false;
            // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSourceBancoDolares = new MatTableDataSource<CajaSoles>(this.CajaBancoDolares);
            this.selectionBancoDolares = new SelectionModel<CajaSoles>(true, []);
            this.dataSourceBancoDolares.sort = this.sortBancoDolares;

            this.isLoadingResults = false;

            this.activeExport = '';

          },
          error => {
            console.log(error.error.text);
            this.loadingbcodolares = false;
          }
      );







    }

  }

  sortDataSoles(sort: Sort) {
    this.loadingsoles = true;
    this.showSoles = false;
    const data = this.CajaSoles.slice();
    if (!sort.active || sort.direction === '') {
      this.CajaSoles = data;
      this.loadingsoles = false;
      this.showSoles = true;
      return;
    } else {
      this.CajaSoles = data.sort((a, b) => {
        const isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'date': return compare(a.date, b.date, isAsc);
          case 'trip': return compare(a.trip, b.trip, isAsc);
          case 'provider': return compare(a.provider, b.provider, isAsc);
          case 'category': return compare(a.category, b.category, isAsc);
          case 'operation_type': return compare(a.operation_type, b.operation_type, isAsc);
          case 'type': return compare(a.type, b.type, isAsc);
          case 'exchange_rate': return compare(a.exchange_rate, b.exchange_rate, isAsc);
          case 'amount': return compare(a.amount, b.amount, isAsc);
          case 'cheque_number': return compare(a.cheque_number, b.cheque_number, isAsc);
          default: return 0;
        }
      });
      // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource = new MatTableDataSource<CajaSoles>(this.CajaSoles);
      this.selection = new SelectionModel<CajaSoles>(true, []);
      this.dataSource.sort = this.sort;
      this.loadingsoles = false;
      this.showSoles = true;
    }
  }

  sortDataDolares(sort: Sort) {
    this.loadingdolares = true;
    this.showDolares = false;
    const data = this.CajaDolares.slice();
    if (!sort.active || sort.direction === '') {
      this.CajaDolares = data;
      this.loadingdolares = false;
      this.showDolares = true;
      return;
    }

    this.CajaDolares = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return compare(a.date, b.date, isAsc);
        case 'trip': return compare(a.trip, b.trip, isAsc);
        case 'provider': return compare(a.provider, b.provider, isAsc);
        case 'category': return compare(a.category, b.category, isAsc);
        case 'operation_type': return compare(a.operation_type, b.operation_type, isAsc);
        case 'type': return compare(a.type, b.type, isAsc);
        case 'amount': return compare(a.amount, b.amount, isAsc);
        case 'exchange_rate': return compare(a.exchange_rate, b.exchange_rate, isAsc);
        case 'cheque_number': return compare(a.cheque_number, b.cheque_number, isAsc);
        default: return 0;
      }
    });

    // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSourceDolares = new MatTableDataSource<CajaSoles>(this.CajaDolares);
    this.selectionDolares = new SelectionModel<CajaSoles>(true, []);
    this.dataSourceDolares.sort = this.sortDolares;
    this.loadingdolares = false;
    this.showDolares = true;
  }

  sortDataBcoSoles(sort: Sort) {
    this.loadingbcosoles = true;
    this.showBancoSoles = false;
    const data = this.CajaBancoSoles.slice();
    if (!sort.active || sort.direction === '') {
      this.CajaBancoSoles = data;
      this.loadingbcosoles = false;
      this.showBancoSoles = true;
      return;
    }

    this.CajaBancoSoles = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return compare(a.date, b.date, isAsc);
        case 'trip': return compare(a.trip, b.trip, isAsc);
        case 'provider': return compare(a.provider, b.provider, isAsc);
        case 'category': return compare(a.category, b.category, isAsc);
        case 'operation_type': return compare(a.operation_type, b.operation_type, isAsc);
        case 'type': return compare(a.type, b.type, isAsc);
        case 'amount': return compare(a.amount, b.amount, isAsc);
        case 'exchange_rate': return compare(a.exchange_rate, b.exchange_rate, isAsc);
        case 'cheque_number': return compare(a.cheque_number, b.cheque_number, isAsc);
        default: return 0;
      }
    });

    // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSourceBancoSoles = new MatTableDataSource<CajaSoles>(this.CajaBancoSoles);
    this.selectionBancoSoles = new SelectionModel<CajaSoles>(true, []);
    this.dataSourceBancoSoles.sort = this.sortBancoSoles;
    this.loadingbcosoles = false;
    this.showBancoSoles = true;
  }

  sortDataBcoDolares(sort: Sort) {
    this.loadingbcodolares = true;
    this.showBancoDolares = false;
    const data = this.CajaBancoDolares.slice();
    if (!sort.active || sort.direction === '') {
      this.CajaBancoDolares = data;
      this.loadingbcodolares = false;
      this.showBancoDolares = true;
      return;
    }

    this.CajaBancoDolares = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return compare(a.date, b.date, isAsc);
        case 'trip': return compare(a.trip, b.trip, isAsc);
        case 'provider': return compare(a.provider, b.provider, isAsc);
        case 'category': return compare(a.category, b.category, isAsc);
        case 'operation_type': return compare(a.operation_type, b.operation_type, isAsc);
        case 'type': return compare(a.type, b.type, isAsc);
        case 'amount': return compare(a.amount, b.amount, isAsc);
        case 'exchange_rate': return compare(a.exchange_rate, b.exchange_rate, isAsc);
        case 'cheque_number': return compare(a.cheque_number, b.cheque_number, isAsc);
        default: return 0;
      }
    });

    // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSourceBancoDolares = new MatTableDataSource<CajaSoles>(this.CajaBancoDolares);
    this.selectionBancoDolares = new SelectionModel<CajaSoles>(true, []);
    this.dataSourceBancoDolares.sort = this.sortBancoDolares;
    this.loadingbcodolares = false;
    this.showBancoDolares = true;
  }

  parseType(x) {
    const list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for (const xx of list_types) {
      if (xx.id === x) {
        return xx.name;
      }
    }
  }

  parseAmount(x) {
    if (Number(x) > 0 ) {
      return '+' + x;
    } else {
      return x;
    }
  }

  FilterDebe(x) {
    if(Number(x) > 0) {
      return x;
    } else {
      return '0.00';
    }
  }

  FilterHaber(x) {
    if(Number(x) < 0) {
      return Number(Number(x)*-1).toFixed(2);
    } else {
      return '0.00';
    }
  }

  parseDate(x:string) {
    return x.split('T')[0];
  }

  FixedNumber(x) {
    return Number(x).toFixed(2);
  }

  CalculateDebe(Caja) {
    let sum = 0;

    for ( const xx of Caja) {
      sum = sum + Number(xx.amount);
    }
    return sum;

  }

  export() {
    let data = new FormData();
    data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
    data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
    data.append('type', '1');
    this.http.post(localStorage.getItem('baseurl') + 'export_cash/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'CuadreCaja_Soles.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
          }
        }
      );

    data = new FormData();
    data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
    data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
    data.append('type', '2');
    this.http.post(localStorage.getItem('baseurl') + 'export_cash/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'CuadreCaja_Dolares.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
          }
        }
      );

    data = new FormData();
    data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
    data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
    data.append('type', '3');
    this.http.post(localStorage.getItem('baseurl') + 'export_cash/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'CuadreCaja_Banco_MN.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
          }
        }
      );

    data = new FormData();
    data.append('date_start', this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day);
    data.append('date_end', this.date2.date.year + '-' + this.date2.date.month + '-' + this.date2.date.day);
    data.append('type', '4');
    this.http.post(localStorage.getItem('baseurl') + 'export_cash/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Caja') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'CuadreCaja_Banco_ME.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
          }
        }
      );
  }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
