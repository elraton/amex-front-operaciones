import { Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ReportProvider } from '../../common/reportprovider';
import { CajaSoles } from '../../common/cajasoles';
import { IMyDpOptions } from 'mydatepicker';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-report-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ReportProveedoresComponent implements OnInit {

  baseimg2: string = localStorage.getItem('baseassets');

  displayedColumns = [
    'date',
    'trip',
    'operation_type',
    'provider',
    'type',
    'amount_s',
    'amount_d',
    'exchange_rate',
    'cheque_number',
    'document_number'
  ];

  dataSource;

  ELEMENT_DATA: ReportProvider[];

  @ViewChild(MatSort) sort: MatSort;

  selection;

  isLoadingResults: boolean;
  showTables: boolean;

  activeExport: string = 'disabled';

  myControl2: FormControl = new FormControl();
  options2 = [];
  filteredOptions2: Observable<string[]>;

  isloading: boolean;
  list_providers: any[];

  providerr: string = '';
  showMessage: boolean;

  idprov: number;
  sumdebe: number;
  sumhaber: number;
  saldo: string;

  isLoading = false;
  showData = false;

  isExporting = false;

  providerName: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) {  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.providerr = params['provider'];
    });

    this.showTables = false;
    this.isloading = true;
    this.showMessage = false;

    this.sumdebe = 0;
    this.sumhaber = 0;

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.list_providers = JSON.parse(aux).providers;

          for (let xx of this.list_providers) {
            this.options2.push(xx.name);
          }

          this.filteredOptions2 = this.myControl2.valueChanges
            .pipe(
              startWith(''),
              map(val => this.filter2(val))
             );

          if (this.providerr !== undefined) {
            this.myControl2.setValue(this.providerr);
            this.generar();
          }
          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

  }

  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  fixnumber(x) {
    return Number(x).toFixed(3);
  }

  FilterDebe(x) {
    if (Number( x) > 0) {
      return x;
    } else {
      return '0.00';
    }
  }

  FilterHaber(x) {
    if (Number(x) < 0) {
      return Number(Number(x) * -1).toFixed(2);
    } else {
      return '0.00';
    }
  }

  openPopup(report: ReportProvider) {
    console.log(report);
    let title = '';
    let conte = '';
    let monto = '';
    let conforme = '';
    let enviado = '';
    let f_enviado = '';
    let moneda = '';
    if (Number(report.amount) >= 0) {
      monto = report.amount;
    } else {
      monto = (Number(report.amount) * -1).toFixed(2);
    }

    if (report.currency === 'S') {
      moneda = 'S/.';
    } else {
      moneda = '$';
    }

    if (report.agree) {
      conforme = 'Si';
    } else {
      conforme = 'No';
    }

    if (report.send) {
      enviado = 'Si';
    } else {
      enviado = 'No';
    }

    if (report.send_date == null) {
      f_enviado = '-';
    } else {
      f_enviado = report.send_date;
    }

    if (report.flag == 'doc') {
      title = 'Documento';
      conte += '<div class="popupReportDoc">';
      conte += '<p><strong>Fecha: </strong> ' + report.date + '</p>';
      conte += '<p><strong>Viaje: </strong> ' + report.trip + '</p>';
      conte += '<p><strong>Moneda: </strong> ' + moneda + '</p>';
      conte += '<p><strong>Monto: </strong> ' + monto + '</p>';
      conte += '<p><strong>Categoria: </strong> ' + report.category + '</p>';
      conte += '<p><strong>Descripción: </strong> ' + report.description + '</p>';
      conte += '<p><strong>Tipo de Documento: </strong> ' + report.document_type + '</p>';
      conte += '<p><strong>Número de Documento: </strong> ' + report.document_number + '</p>';
      conte += '<p><strong>Proveedor: </strong> ' + report.provider + '</p>';
      conte += '<p><strong>Proveedor Origen: </strong> ' + report.provider_origin + '</p>';
      conte += '<p><strong>Conforme: </strong> ' + conforme + '</p>';
      conte += '<p><strong>Enviado: </strong> ' + enviado + '</p>';
      conte += '<p><strong>Fecha Enviado: </strong> ' + f_enviado + '</p>';
      conte += '</div>';
    } else {
      title = 'Movimiento';
      conte += '<div class="popupReportDoc">';
      conte += '<p><strong>Fecha: </strong> ' + report.date + '</p>';
      conte += '<p><strong>Viaje: </strong> ' + report.trip + '</p>';
      conte += '<p><strong>Moneda: </strong> ' + moneda + '</p>';
      conte += '<p><strong>Monto: </strong> ' + monto + '</p>';
      conte += '<p><strong>Tipo de Cambio: </strong> ' + report.exchange_rate + '</p>';
      conte += '<p><strong>Nmro de Cheque: </strong> ' + report.cheque_number + '</p>';
      conte += '<p><strong>Tipo de Caja: </strong> ' + report.type + '</p>';
      conte += '<p><strong>Categoria: </strong> ' + report.category + '</p>';
      conte += '<p><strong>Descripción: </strong> ' + report.description + '</p>';
      conte += '<p><strong>Tipo de Operación: </strong> ' + report.operation_type + '</p>';
      conte += '<p><strong>Proveedor: </strong> ' + report.provider + '</p>';
      conte += '</div>';
    }

    this.modalService.openDialog(this.viewRef, {
      title: title,
      childComponent: SimpleModalComponent,
      data: {
        text: conte
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: []
    });
  }

  generar() {
    this.showMessage = false;
    this.showTables = false;

    this.isLoading = true;
    this.showData = false;

    this.sumdebe = 0;
    this.sumhaber = 0;
    this.saldo = '0';

    this.idprov = 0;

    if (this.myControl2.value !== null || this.myControl2.value !== '') {
      for (let xx of this.list_providers) {
        if (xx.name === this.myControl2.value) {
          this.providerName = this.myControl2.value;
          this.idprov = xx.id;
        }
      }
    }


    if ( this.idprov !== 0 ) {
      const data = new FormData();
      data.append('id_provider', this.idprov.toString());
      this.isLoadingResults = true;

      this.http.post(localStorage.getItem('baseurl') + 'report_providers/', data)
        .subscribe(
          res => {
            this.ELEMENT_DATA = [];
            this.ELEMENT_DATA = JSON.parse(JSON.stringify(res)).objs;

            for (const xx of this.ELEMENT_DATA) {
              if (Number(xx.amount) >= 0) {
                this.sumdebe = this.sumdebe + Number(xx.amount);
              } else {
                this.sumhaber = this.sumhaber + (Number(xx.amount) * -1);
              }
            }

            this.saldo = Number(this.sumdebe - this.sumhaber).toFixed(3);

            this.activeExport = '';

            this.isLoadingResults = false;
            // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSource = new MatTableDataSource<ReportProvider>(this.ELEMENT_DATA);
            this.selection = new SelectionModel<ReportProvider>(true, []);
            this.dataSource.sort = this.sort;

            this.showTables = true;

            this.isLoading = false;
            this.showData = true;
            if (this.ELEMENT_DATA.length < 1) {
              this.isLoading = false;
              this.showData = false;
            }
          },
          error => {
            this.showMessage = true;
            console.log(error.error.text);
            this.isLoading = false;
            this.showData = false;
          }
      );
    }
  }

  parseType(x) {
    const list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for (let xx of list_types) {
      if (xx.id === x) {
        return xx.name;
      }
    }
  }

  parseCurrency(x) {
    if (x === 'D') {
      return '$';
    }
    if (x === 'S') {
      return 'S/.';
    }
  }

  createTable(data) {
  }

  export() {
    this.isExporting = true;
    let data = new FormData();
    data.append('id_provider', this.idprov.toString());
    this.http.post(localStorage.getItem('baseurl') + 'export_report_provider/', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
          if (error.error.text.indexOf('Proveedor') !== -1) {
            const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
            const dwldLink = document.createElement('a');
            const url = URL.createObjectURL(blob);
            const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
            if (isSafariBrowser) {
              dwldLink.setAttribute('target', '_blank');
            }
            dwldLink.setAttribute('href', url);
            dwldLink.setAttribute('download', 'Reporte_Proveedores.csv');
            dwldLink.style.visibility = 'hidden';
            document.body.appendChild(dwldLink);
            dwldLink.click();
            document.body.removeChild(dwldLink);
            this.isExporting = false;
          }
        }
      );
  }

}
