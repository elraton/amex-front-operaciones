import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ReportesComponent } from './reportes.component';
import { CuadreCajaComponent } from './cuadrecaja/cuadrecaja.component';
import { AdelantosComponent } from './adelantos/adelantos.component';
import { PorRendirComponent } from './porrendir/porrendir.component';
import { PorPagarComponent } from './porpagar/porpagar.component';
import { ReportProveedoresComponent } from './proveedores/proveedores.component';
import { ReportesRoutes } from './reportes.routing';
import { SharedModule } from '../shared/shared.module';

import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip';

import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ReportesRoutes),
      SharedModule,
      MatTabsModule,
      MatTableModule,
      MatSortModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MyDatePickerModule,
      MatAutocompleteModule,
      MatInputModule,
      MatTooltipModule
  ],
  declarations: [
    ReportesComponent,
    AdelantosComponent,
    CuadreCajaComponent,
    ReportProveedoresComponent,
    PorRendirComponent,
    PorPagarComponent
  ]
})

export class ReportesModule {}
