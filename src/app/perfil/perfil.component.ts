import {Component, OnInit} from '@angular/core';
declare const $: any;
declare var Morris: any;

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent implements OnInit {

  username: string;
  email: string;
  role: string;
  photo: string;
  photo_file: any;

  baseimg: string = localStorage.getItem('baseassets');
  baseimg2: string = localStorage.getItem('baseimg');

  isChangePass: boolean = false;

  oldpass: string;
  newpass: string;
  newpassconfirm: string;

  constructor(
    private http: HttpClient,
    public router: Router,
    private location: Location,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
  ) { this.toastyConfig.theme = 'bootstrap'; }

  ngOnInit() {
    let username = localStorage.getItem('username');

    let formData = new FormData();

    formData.append("username", username);

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          this.username = JSON.parse(JSON.stringify(res)).username;
          const rol = JSON.parse(JSON.parse(JSON.stringify(res)).role);
          this.role = rol[0].fields.name;
          this.photo = JSON.parse(JSON.stringify(res)).photo;
          this.email = JSON.parse(JSON.stringify(res)).email;

          if (this.photo === '') {
            this.photo = 'assets/images/user.png';
            this.baseimg2 = this. baseimg;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  changePass() {
    this.isChangePass = true;
  }
  save() {
    let form = new FormData();
    form.append('username', this.username);
    form.append('email', this.email);
    form.append('rol', this.role);
    form.append('photo', this.photo_file);

    this.http.post(localStorage.getItem('baseurl') + 'modifyuser/', form)
      .subscribe(
        res => {
          console.log(res);
          localStorage.setItem('username', JSON.parse(JSON.stringify(res)).username);
          this.toastyService.success('Se guardo exitosamente');
        },
        error => {
          this.toastyService.error('Ocurrio un error');
          console.log(error);
        }
      );
  }
  cancel() {
    this.location.back();
  }

  savePass() {
    if (this.newpassconfirm === this.newpass) {
      const form = new FormData();
      form.append('username', this.username);
      form.append('old_password', this.oldpass);
      form.append('new_password', this.newpass);

      this.http.post(localStorage.getItem('baseurl') + 'changepassword/', form)
        .subscribe(
          res => {
            console.log(res);
            this.toastyService.success('Se guardo exitosamente');
            this.newpass = '';
            this.newpassconfirm = '';
            this.oldpass = '';
            setTimeout(() => {
              this.isChangePass = false;
            }, 2000);
          },
          error => {
            this.toastyService.error('Contraseña anterior incorrecta');
            this.newpass = '';
            this.newpassconfirm = '';
            this.oldpass = '';
            console.log(error);
          }
        );

    } else {
      this.toastyService.warning('Confirme correctamente la nueva contraseña');
    }
  }
  cancelPass() {
    this.isChangePass = false;
  }
}
