import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { Trip } from '../../common/trip';
import { Document } from '../../common/document';
import { Register } from '../../common/register';
import {IMyDpOptions} from 'mydatepicker';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  selector: 'app-add-viajes',
  templateUrl: './addregistro.component.html',
  styleUrls: ['./addregistro.component.css']
})


export class AddRegistroComponent implements OnInit {

  myControl: FormControl = new FormControl();
  myControl2: FormControl = new FormControl();
  myControl3: FormControl = new FormControl();

  options = [];
  options2 = [];
  options3 = [];

  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;
  filteredOptions3: Observable<string[]>;

  list_trips: any[];
  list_providers: any[];
  list_tip_docs: any[];
  list_categories: any[];

  TripListStrings: string[];
  tripText: any;

  register: Document;

  rol = JSON.parse(localStorage.getItem('rol'));
  enableagree = this.rol.doc_agree;
  enablesend = this.rol.doc_send;

  photo_base_64: string;
  photo_file: File;
  showImg: boolean;

  viajeError = false;

  debehaber: number;

  isloading: boolean;
  isSaved: boolean;

  baseimg2: string = localStorage.getItem('baseassets');

  isUploading = false;

  showproviderinfo = false;
  ruc_aux = '';
  razon_aux = '';

  showproviderinfo2 = false;
  ruc_aux2 = '';
  razon_aux2 = '';

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptionsSend: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px',
    componentDisabled: !this.enablesend
  };



  public date1: any;
  public date2: any;

  document_send = 2;
  document_agree = 2;

  document_photo_loading = false;
  document_photo_show = false;
  document_photo_base64 = '';
  document_photo_file: any;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) {
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {

    this.isloading = true;
    this.isSaved = false;

    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1; // January is 0!
    const yyyy = today.getFullYear();

    this.debehaber = 0;

    this.date1 = { date: { year: yyyy, month: mm, day: dd } };

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
      .subscribe(
        res => {
          const aux = JSON.stringify(res);
          localStorage.setItem('trip_types', JSON.stringify(JSON.parse(aux).trip_types));
          localStorage.setItem('operation_types', JSON.stringify(JSON.parse(aux).operation_types));
          localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
          localStorage.setItem('document_types', JSON.stringify(JSON.parse(aux).document_types));

          this.list_tip_docs = JSON.parse(localStorage.getItem('document_types'));
          this.list_categories = JSON.parse(localStorage.getItem('categories'));
        },
        error => {
          console.log(error.error.text);
        }
      );

    this.list_tip_docs = JSON.parse(localStorage.getItem('document_types'));
    this.list_categories = JSON.parse(localStorage.getItem('categories'));

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          const aux = JSON.stringify(res);
          this.list_trips = JSON.parse(aux).trips;
          this.list_providers = JSON.parse(aux).providers;

          for (const xx of this.list_trips) {
            this.options.push(xx.code);
          }

          for (const xx of this.list_providers) {
            this.options2.push(xx.name);
          }

          this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter(val))
          );

        this.filteredOptions2 = this.myControl2.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.filteredOptions3 = this.myControl3.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

      this.showImg = false;

      // 1 - true // 2 - false
      this.document_send = 2;
      this.document_agree = 2;

      this.cleanForm();
  }

  TipoDocChange() { }

  providerChange(yy) {
    this.myControl3.setValue(this.myControl2.value);
    /* buscar proveedor y luego asignarle la categoria */
    this.showproviderinfo = false;
    for (const xx of this.list_providers) {
      if (xx.name === this.myControl2.value) {
        if (xx.category > 0) {
          this.register.category = xx.category;
        }
        this.ruc_aux = xx.ruc;
        if ( xx.socialreason !== 'undefined' && xx.socialreason) {
          this.razon_aux = xx.socialreason;
        }
        this.showproviderinfo = true;

        this.ruc_aux2 = xx.ruc;
        if ( xx.socialreason !== 'undefined' && xx.socialreason) {
          this.razon_aux2 = xx.socialreason;
        }
        this.showproviderinfo2 = true;
      }
    }
  }

  providerChange2(yy) {
    /* buscar proveedor y luego asignarle la categoria */
    this.showproviderinfo2 = false;
    for (const xx of this.list_providers) {
      if (xx.name === this.myControl3.value) {
        this.ruc_aux2 = xx.ruc;
        if ( xx.socialreason !== 'undefined' && xx.socialreason) {
          this.razon_aux2 = xx.socialreason;
        }
        this.showproviderinfo2 = true;
      }
    }
  }

  filter(val: string): string[] {
    const arr = this.options.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
    if ( arr.length > 0 ) {
      this.viajeError = false;
      return arr;
    } else {
      this.viajeError = true;
      return ['No existe el viaje'];
    }
  }
  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  debeChange() {
    if (this.debehaber === 0) {
      this.register.amount = '0';
    }
    if (this.debehaber === 1) {
      this.register.amount = '0';
    }
  }

  exchangeChange() {
    this.register.exchange_rate = Number(this.register.exchange_rate).toFixed(2);
  }

  montoChange() {
    this.register.amount = Number(this.register.amount).toFixed(2);
  }

  onChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      this.document_photo_file = event.target.files[0];
      reader.readAsDataURL(this.document_photo_file);
      reader.onload = () => {
        this.document_photo_base64 = reader.result;
        this.showImg = true;
      };

    }
  }

  numberChange(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode === 46) {

    } else {
      if (event.keyCode === 32 || event.keyCode === 8 || !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }

  /*autoChange(event: any) {
    this.tripText = event;
    this.trip = '';
    for(let xx of this.list_trips) {
      if( xx.code === this.tripText) {
        this.trip = xx.code;
      }
    }

    if(this.trip == ''){
      this.tripText = '';
    }
  }*/

  canDeactivate() {
    console.log('i am navigating away');
    if (this.isSaved === false) {
      return window.confirm('¿Realmente Desea Salir?');
    }
    return true;
  }

  toggleCurrency() {
    if (this.register.currency === 'S') {
      this.register.currency = 'D';
    } else {
      this.register.currency = 'S';
    }
  }

  cleanForm() {
    this.register = {
      id: 0,
      trip: '',
      date: '',
      category: '0',
      provider: '',
      provider_origin: '',
      send: '',
      send_date: '',
      document_type: '0',
      document_number: '',
      document_photo: '',
      description: '',
      currency: 'S',
      amount: '',
      agree: '',
      exchange_rate: ''
    };
    this.isSaved = false;
    this.myControl.setValue('');
    this.myControl2.setValue('');
    this.myControl3.setValue('');

    /*const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();

    this.date1 = { date: { year: yyyy, month: mm, day: dd } };*/
  }

  save(opt: Number) {

    let error = false;
    this.register.trip = '';
    if(this.myControl.value !== null || this.myControl.value !== ''){
      for(let xx of this.list_trips){
        if (xx.code === this.myControl.value) {
          this.register.trip = xx.id;
        }
      }
    }

    if (this.register.trip === '') {
      this.toastyService.warning('No existe el viaje');
      error = true;
    }

    this.register.provider = '';
    if (this.myControl2.value !== null || this.myControl2.value !== '') {
      for (let xx of this.list_providers) {
        if (xx.name === this.myControl2.value) {
          this.register.provider = xx.id;
        }
      }
    }

    if (this.register.provider === '') {
      this.toastyService.warning('No existe el proveedor');
      error = true;
    }

    this.register.provider_origin = '';
    if (this.myControl3.value !== null || this.myControl3.value !== '') {
      for (const xx of this.list_providers) {
        if (xx.name === this.myControl3.value) {
          this.register.provider_origin = xx.id;
        }
      }
    }

    if (this.register.provider_origin === '') {
      this.toastyService.warning('No existe el proveedor origen');
      error = true;
    }

    if (this.register.description === '') {
      this.toastyService.warning('Debe ingresar una descripción');
      error = true;
    }

    if (this.register.amount === '') {
      this.toastyService.warning('Ingrese Debe / Haber');
      error = true;
    }

    if (this.register.category === '0') {
      this.toastyService.warning('Ingrese una Categoria');
      error = true;
    }

    if (this.date1.formatted !== undefined) {
      // this.register.date = this.date1.formatted;
      const dat = this.date1.formatted.split('/');
      this.register.date = dat[2] + '-' + dat[1] + '-' + dat[0];
    } else {
      this.register.date = this.date1.date.year + '-' + this.date1.date.month + '-' + this.date1.date.day;
    }

    if (this.date2 !== undefined) {
      if (this.date2.formatted !== undefined) {
        this.register.send_date = this.date2.formatted;
      }
    }

    this.document_send == 2 ? this.register.send = '0' : this.register.send = '1';
    this.document_agree == 2 ? this.register.agree = '0' : this.register.agree = '1';

    if (Number(this.register.amount) === 0) {
      this.toastyService.warning('Ingrese un monto valido');
      error = true;
    }

    if (error === false) {

      for (const dd of this.list_tip_docs) {
        if (dd.id === +this.register.document_type) {
          if (dd.name.toLowerCase().indexOf('nota') !== -1) {
            if (Number(this.register.amount) > 0) {
              this.register.amount = (Number(this.register.amount) * -1).toFixed(2);
            }
          }
        }
      }

      const formData = new FormData();
      const hour = new Date().getHours();
      const min = new Date().getMinutes();
      const sec = new Date().getSeconds();

      formData.append('trip', this.register.trip);
      formData.append('date', this.register.date + 'T' + hour + ':' + min + ':' + sec);
      formData.append('category', this.register.category);
      formData.append('provider', this.register.provider);
      formData.append('provider_origin', this.register.provider_origin);
      formData.append('description', this.register.description);
      formData.append('currency', this.register.currency);
      formData.append('amount', this.register.amount);
      formData.append('exchange_rate', this.register.exchange_rate);
      formData.append('send', this.register.send);
      formData.append('send_date', this.register.send_date);
      formData.append('agree', this.register.agree);
      formData.append('document_type', this.register.document_type);
      formData.append('document_number', this.register.document_number);
      formData.append('document_photo', this.document_photo_file);

      this.http.post(localStorage.getItem('baseurl') + 'list_documents/add/', formData)
        .subscribe(
          res => {
            console.log(res);
            this.toastyService.success('Registro creado');
              this.isSaved = true;
              if (opt === 1) {
                setTimeout(() => {
                  this.location.back();
                }, 1000);
              } else {
                this.cleanForm();
              }
          },
          error2 => {
            this.toastyService.error('Ocurrio un error');
            console.log(error2);
          }
        );

    }


  }

  cancelar() {
    this.location.back();
  }

}
