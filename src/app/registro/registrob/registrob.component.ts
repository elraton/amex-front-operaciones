import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Page } from '../../common/page';
import { Register } from '../../common/register';
import {IMyDpOptions, IMyInputFocusBlur, IMySelector} from 'mydatepicker';
import {MatSort, MatTableDataSource} from '@angular/material';
import {RegistroAux} from '../../common/registroaux';
import {SelectionModel} from '@angular/cdk/collections';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './registrob.component.html',
  styleUrls: ['./registrob.component.css']
})
export class RegistroBComponent implements OnInit {

  displayedColumns = [
    'select',
    'trip',
    'date',
    'category',
    'provider',
    'document_type',
    'document_number',
    'amount_s',
    'amount_d',
    'exchange',
    'agree',
    'send',
    'aux'
  ];

  export_message: string = 'Exportar Todo';

  dataSource;

  ELEMENT_DATA;

  @ViewChild(MatSort) sort: MatSort;

  selection;

  isLoadingResults: boolean;

  fecha: string;
  proveedores: string;
  categoria: string;
  viaje: string;
  enviado: string;
  moneda: string;

  Registros: Register[];
  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  filterSelect: number;

  dates1: string;
  datee1: string;

  proveedoresv: string;
  categoriav: string;
  viajev: string;
  enviadov: string;
  monedav: string;

  proveedores_list: any[];
  categoria_list: any[];
  trip_list: any[];

  showNotFound: string;
  activeExport: string;
  conformev = 1;

  isLoading = true;
  showData = false;
  isExporting = false;

  fechasFilter = 0;

  send_button = true;
  agree_button = false;

  private selector: IMySelector = {
    open: false
  };
  private selector2: IMySelector = {
    open: false
  };

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptions2: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;
  public date3: any;
  public date4: any;

  baseimg2 = localStorage.getItem('baseassets');

  rol = JSON.parse(localStorage.getItem('rol'));
  showadd;
  showview;
  showedit;
  showdelete;
  showexport;

  items: Number;
  itemspertable = 20;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    ) { }

  ngOnInit() {

    const formData = new FormData();
    formData.append('username', localStorage.getItem('username'));

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          this.rol = JSON.parse(JSON.parse(JSON.stringify(res)).role)[0].fields;
          localStorage.setItem('rol', JSON.stringify(this.rol));
          this.showadd = this.rol.doc_add;
          this.showview = this.rol.doc_view;
          this.showedit = this.rol.doc_edit;
          this.showdelete = this.rol.doc_delete;
          this.showexport = this.rol.doc_export;
        },
        error => {
          console.log(error);
        }
      );

    this.isLoadingResults = true;
    this.activeExport = 'disabled';

    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          localStorage.setItem('trip_types', JSON.stringify(JSON.parse(aux).trip_types));
          localStorage.setItem('operation_types', JSON.stringify(JSON.parse(aux).operation_types));
          localStorage.setItem('categories', JSON.stringify(JSON.parse(aux).categories));
          localStorage.setItem('document_types', JSON.stringify(JSON.parse(aux).document_types));
        },
        error => {
          console.log(error.error.text);
        }
      );

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.proveedores_list = JSON.parse(aux).providers;
          this.trip_list = JSON.parse(aux).trips;
        },
        error => {
          console.log(error.error.text);
        }
      );


    this.categoria_list = JSON.parse(localStorage.getItem('categories'));

    this.fecha = 'no-display';
    this.proveedores = 'no-display';
    this.categoria = 'no-display';
    this.viaje = 'no-display';
    this.enviado = 'no-display';
    this.moneda = 'no-display';

    this.showNotFound = 'hide-loading';
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;

    this.proveedoresv = '0';
    this.categoriav = '0';
    this.viajev = '0';
    this.enviadov = '1';
    this.monedav = 'S';


    this.route.params.subscribe(params => {
      this.page = +params['p'];
    });

    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }


    this.http.get(localStorage.getItem('baseurl') + 'list_documents/?page=' + this.page)
      .subscribe(
        res => {

          let aux = JSON.stringify(res);

          this.totalpages = JSON.parse(aux).num_pages;
          this.Registros = JSON.parse(aux).objects;
          this.items = JSON.parse(aux).items;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          if(this.Registros.length < 0 ){
            this.showNotFound = 'show-loading';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';

          this.isLoading = false;
          this.showData = true;
        },
        error => {
          console.log(localStorage.getItem('baseurl'));
          console.log(error.error.text);
          this.isLoading = false;
          this.showData = false;
        }
      );
  }

  onInputFocusBlur2(event: IMyInputFocusBlur): void {
    console.log(this.date2);
    if ( event.reason === 1) {
      if (this.date2 === undefined || this.date2 === null ) {
        this.selector2 = {
          open: true
        };
      } else {
        if ( this.date2.formatted === '' ) {
          this.selector2 = {
            open: true
          };
        }
      }
    }
    if ( event.reason === 2) {
      if (this.date2 !== undefined && this.date2 !== null) {
        this.selector2 = {
          open: false
        };
      }
    }
  }

  onInputFocusBlur(event: IMyInputFocusBlur): void {
    if ( event.reason === 1) {
      this.selector = {
        open: true
      };
    }
    if ( event.reason === 2) {
      if (this.date1) {
        this.selector = {
          open: false
        };
      }
    }
  }

  stripDate(str: string) {
    const dat = new Date(str.split('T')[0]);
    return dat;
  }

  parseDocType(str: string) {
    return str.slice(0, 10) + '...';
  }

  setRows() {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.isLoading = true;
    this.showData = false;

    this.page = 1;
    this.location.replaceState('/registro/' + this.page);
    this.http.get(localStorage.getItem('baseurl') + 'list_documents/?page=' + this.page + '&items=' + this.itemspertable)
      .subscribe(
        res => {

          let aux = JSON.stringify(res);

          this.totalpages = JSON.parse(aux).num_pages;
          this.Registros = JSON.parse(aux).objects;
          this.items = JSON.parse(aux).items;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          if(this.Registros.length < 0 ){
            this.showNotFound = 'show-loading';
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';

          this.isLoading = false;
          this.showData = true;

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;
        },
        error => {
          console.log(localStorage.getItem('baseurl'));
          console.log(error.error.text);
          this.isLoading = false;
          this.showData = false;
        }
      );
  }

  send_list() {

  }

  agree_list() {
    if (this.selection.hasValue()) {
      const data = [];
      for (let xx of this.selection.selected) {
        data.push(xx.id);
      }

      const form = new FormData();
      form.append('type', '7');
      form.append('documents', data.join(','));
      form.append('date_start', '');
      form.append('date_end', '');
      form.append('date_ini_send', '');
      form.append('date_end_send', '');

      this.http.post(localStorage.getItem('baseurl') + 'export_documents/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.status === 500) {
              this.toastyService.error('No se pudo exportar');
              this.isExporting = false;
              return;
            }
            if (error.error.text.indexOf('Viaje') !== -1){
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'documentos_seleccionados.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
              this.isExporting = false;
            }
          });


    }
  }

  export() {
    this.isExporting = true;
    if (this.selection.hasValue()) {
      const data = [];
      for (let xx of this.selection.selected) {
        data.push(xx.id);
      }

      const form = new FormData();
      form.append('type', '7');
      form.append('documents', data.join(','));
      form.append('date_start', '');
      form.append('date_end', '');
      form.append('date_ini_send', '');
      form.append('date_end_send', '');

      this.http.post(localStorage.getItem('baseurl') + 'export_documents/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.status === 500) {
              this.toastyService.error('No se pudo exportar');
              this.isExporting = false;
              return;
            }
            if (error.error.text.indexOf('Viaje') !== -1){
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'documentos_seleccionados.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
              this.isExporting = false;
            }
          });


    } else {
      const data = new FormData();
      data.append('type', '0');
      data.append('date_start', '');
      data.append('date_end', '');
      data.append('date_ini_send', '');
      data.append('date_end_send', '');
      this.http.post(localStorage.getItem('baseurl') + 'export_documents/', data)
        .subscribe(
          response => {
            console.log(response);
          },
          error => {
            console.log(error);
            if (error.status === 500) {
              this.toastyService.error('No se pudo exportar');
              this.isExporting = false;
              return;
            }
            if (error.error.text.indexOf('Viaje') !== -1) {
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'documentos.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
              this.isExporting = false;
            }
          });
    }
  }

  selectChange(e: any, row: any, select: any) {
    e ? select.toggle(row) : null;

    if (select.hasValue()) {
      this.export_message = 'Exportar Seleccionados';
    } else {
      this.export_message = 'Exportar Todo';
    }
  }

   /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(select: any, datas) {
    const numSelected = select.selected.length;
    const numRows = datas.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(select, datas) {
    if (select.hasValue()) {
      this.export_message = 'Exportar Todo';
    } else {
      this.export_message = 'Exportar Seleccionados';
    }
    this.isAllSelected(select, datas) ?
      select.clear() :
      datas.data.forEach(row => select.select(row));
  }

  parseType(x) {
    let list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for(let xx of list_types) {
      if(xx.id == x) {
        return xx.name;
      }
    }
  }
  createTableRegisters() {
    this.ELEMENT_DATA = this.Registros;
    this.isLoadingResults = false;
    this.dataSource = new MatTableDataSource<Register>(this.ELEMENT_DATA);
    this.selection = new SelectionModel<Register>(true, []);
    this.dataSource.sort = this.sort;
  }

  fechas() {
    if (this.fechasFilter !== 0) {
      switch (+this.fechasFilter) {
        case 1: {
          const today = new Date();
          this.date1 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date2 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 2: {
          const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
          this.date2 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 3: {
          const today = new Date();
          const week = today.getDay();
          const monday = week * 24 * 60 * 60 * 1000;
          const saturday = (6 - week) * 24 * 60 * 60 * 1000;
          const mon_date = new Date(today.getTime() - monday);
          const sat_date = new Date(today.getTime() + saturday);
          this.date1 = { date: { year: mon_date.getFullYear(), month: mon_date.getMonth() + 1, day: mon_date.getDate() } };
          this.date2 = { date: { year: sat_date.getFullYear(), month: sat_date.getMonth() + 1, day: sat_date.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 5: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), 0, 1);
          const last_day = new Date(today.getFullYear(), 11, 31);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  changePage(p) {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.isLoading = true;
    this.showData = false;
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/registro/"+this.page);

    this.http.get(localStorage.getItem('baseurl') + 'list_documents?page='+this.page)
      .subscribe(
        res => {
          let aux = JSON.stringify(res);

          this.Registros = JSON.parse(aux).objects;

          this.createTableRegisters();
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;
        },
        error => {
          console.log(error.error.text);
          this.isLoading = false;
          this.showData = false;
        }
      );
  }

  editTrip(v) {
    if (this.showedit) {
      let vv;
      for (let xx of this.Registros) {
        if (xx.id === v) {
          vv = xx;
          localStorage.setItem("edittrip", JSON.stringify(vv));
          this.router.navigate(['/registro/edit/'+v]);
          return;
        }
      }
    }
  }
  viewTrip(v) {
    let vv;
    for (let xx of this.Registros) {
      if (xx.id === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/registro/view/'+v]);
        return;
      }
    }
  }

  deleteTrip(v) {
    console.log(v);
    let vv;
    for (let xx of this.Registros) {
      if (xx.id === v) {
        vv = xx;
        break;
      }
    }
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Registro?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const varData = new FormData();
              varData.append('id', v);
              this.http.post(localStorage.getItem('baseurl') + 'list_documents/delete/', varData)
                .subscribe(
                  res => {
                    this.Registros = this.Registros.filter(obj => obj !== vv);
                    this.isLoadingResults = true;
                    this.createTableRegisters();
                    resolve();
                  },
                  error => {
                    console.log(error.error.text);
                    resolve();
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }
  activeTrip(v) {
    for(let x of this.Registros) {
      x.active = '';
    }
    v.active = 'active';
  }

  setTimeDisable(event) {
    if (event.formatted === '') {
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: 0, month: 0, day: 0}
      };
    } else {
      const aa = event.formatted.split('-');
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: Number(aa[0]), month: Number(aa[1]), day: Number(aa[2])}
      };
    }
  }

  filtrar() {
    let dateIni = '';
    let dateFin = '';
    if (this.date1 === undefined || this.date2 === undefined) {
      if (this.filterSelect === 0) {
        this.toastyService.error('Seleccione fechas validas');
        return;
      }
    } else {
      if (this.date1.formatted === undefined || this.date2.formatted === undefined) {
        if (this.filterSelect === 0) {
          this.toastyService.error('Seleccione fechas validas');
          return;
        }
      } else {
        dateIni = this.date1.formatted;
        dateFin = this.date2.formatted;
      }
    }

    let dateSendIni = '';
    let dateSendFin = '';
    if (this.date3 !== undefined && this.date4 !== undefined) {
      if (this.date3.formatted !== undefined && this.date4.formatted !== undefined) {
        dateSendIni = this.date3.formatted;
        dateSendFin = this.date4.formatted;
      }
    }

    switch (+this.filterSelect) {
      case 0: {
        localStorage.setItem('dateIni', dateIni);
        localStorage.setItem('dateFin', dateFin);
        this.router.navigate(['/registro/filter/0/0/1']);
        break;
      }
      case 1: {
        localStorage.setItem('dateIni', dateIni);
        localStorage.setItem('dateFin', dateFin);
        this.router.navigate(['/registro/filter/1/' + this.conformev + '/1']);
        break;
      }
      case 2: {
        let error = false;

        if (this.proveedoresv === '0') {
          this.toastyService.error('Seleccione un proveedor');
          error = true;
        }

        if (error == false) {
          localStorage.setItem('dateIni', dateIni);
          localStorage.setItem('dateFin', dateFin);
          this.router.navigate(['/registro/filter/2/' + this.proveedoresv + '/1']);
        }
        break;
      }
      case 3: {
        let error = false;
        if (this.categoriav === '0') {
          this.toastyService.error('Seleccione una categoria');
          error = true;
        }
        if (error == false) {
          localStorage.setItem('dateIni', dateIni);
          localStorage.setItem('dateFin', dateFin);
          this.router.navigate(['/registro/filter/3/' + this.categoriav + '/1']);
        }
        break;
      }
      case 4: {
        let error = false;
        if (this.viajev === '0') {
          this.toastyService.error('Seleccione un viaje');
          error = true;
        }
        if (error === false) {
          localStorage.setItem('dateIni', dateIni);
          localStorage.setItem('dateFin', dateFin);
          this.router.navigate(['/registro/filter/4/' + this.viajev + '/1']);
        }
        break;
      }
      case 5: {
        localStorage.setItem('dateIni', dateIni);
        localStorage.setItem('dateFin', dateFin);

        localStorage.setItem('dateSendIni', dateSendIni);
        localStorage.setItem('dateSendFin', dateSendFin);
        this.router.navigate(['/registro/filter/5/' + this.enviadov + '/1']);
        break;
      }
      case 6: {
        localStorage.setItem('dateIni', dateIni);
        localStorage.setItem('dateFin', dateFin);
        this.router.navigate(['/registro/filter/6/' + this.monedav + '/1']);
        break;
      }
    }
  }

  parseProvider(ss: string) {
    return ss.slice(0, 7) + '...';
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

  filtrarForm(e, val) {
    switch (val) {
      case '0': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.enviado = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '1': {
        this.fecha = '';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.enviado = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '2': {
        this.fecha = 'no-display';
        this.proveedores = '';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.enviado = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '3': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = '';
        this.viaje = 'no-display';
        this.enviado = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '4': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = '';
        this.enviado = 'no-display';
        this.moneda = 'no-display';
        break;
      }
      case '5': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.enviado = '';
        this.moneda = 'no-display';
        break;
      }
      case '6': {
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        this.viaje = 'no-display';
        this.enviado = 'no-display';
        this.moneda = '';
        break;
      }
    }
  }

}
