import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RegistroComponent } from './registro.component';
import { RegistroRoutes } from './registro.routing';
import {SharedModule} from '../shared/shared.module';

import { RegistroBComponent } from './registrob/registrob.component';
import { AddRegistroComponent } from './addregistro/addregistro.component';
import { EditRegistroComponent } from './editregistro/editregistro.component';
import { ViewRegistroComponent } from './viewregistro/viewregistro.component';
import { FilterregistroComponent } from './filter/filterregistro.component';

import { MyDatePickerModule } from 'mydatepicker';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CanDeactivateGuard } from '../preventexit/prevent';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RegistroRoutes),
      SharedModule,
      MyDatePickerModule,
      MatTableModule,
      MatSortModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MatAutocompleteModule,
      MatInputModule,
      MatTooltipModule
  ],
  declarations: [
    RegistroComponent,
    RegistroBComponent,
    AddRegistroComponent,
    EditRegistroComponent,
    ViewRegistroComponent,
    FilterregistroComponent
  ],
  providers: [
      CanDeactivateGuard
  ]
})

export class RegistroModule {}
